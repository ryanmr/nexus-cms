The Nexus
=========

This is The Nexus *content management system*.

This is written with the following tools:

* PHP 5.6+ and Laravel 5.2
* JavaScript
  - jQuery
  - Vue.js
* CSS
  - Foundation

There are other dependancies. Watch out for those, they bite.

Setup
-----

Suppose you wanted to set this up on the *production* environment (because the *development* environment is within Homestead), you might follow these steps.

1. Commit the changes to the remote repository as the release branch
2. Pull the release branch on the *production* server
3. Run `composer dumpautoload`, `php artisan optimize`, `gulp --production`
4. Verify everything works (maybe run the tests? or not)
5. Enjoy the updates (if it works)
6. Otherwise, revert!

These steps are not strictly correct, but an idea of what to do.
