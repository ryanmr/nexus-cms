<html>
	<head>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #222;
				display: table;
				font-weight: 100;
				font-family: sans-serif;
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 96px;
				margin-bottom: 40px;
			}

			.title span {
				font-family: "Baskerville", serif;
			}

			.quote {
				font-size: 24px;
			}

			.episodes {
				width: 68%;
				text-align: left;
				margin: 1em auto;
			}

			h3 {
				font-size: 1.5em;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">Welcome to <span>The Nexus</span>!</div>

				<div>
					View all the series!
					<a href="/series">Go!</a>
				</div>

				<div class="episodes">
				@foreach ($episodes as $episode)
					<div class="episode">
						<h3><a href="{{ route('episode', [$episode->series->slug, $episode->number]) }}">{{ $episode->formal_title }}</a></h3>
						<p>{{ strip_tags($episode->description) }}</p>
					</div>
				@endforeach
				</div>

			</div>
		</div>
	</body>
</html>
