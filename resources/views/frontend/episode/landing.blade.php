@extends('frontend.layout')

@section('content')

<div>

  <h1>{{$episode->series->name}}</h1>
  <h2>#{{$episode->number}}: {{$episode->title}}</h2>
  @if ($episode->subtitle)
    <h4>{{$episode->subtitle}}</h4>
  @endif

  <p>
    {!! $episode->content !!}
  </p>

  @if(!$episode->people->isEmpty())
  <h3>Who's In It?</h3>
    <ul>
      @foreach($episode->people as $person)
        <li>{{$person->name}}</li>
      @endforeach
    </ul>
  @endif

  @if(!$episode->media->isEmpty())
  <hr>
  <h3>Media</h3>
    <ul>
      @foreach($episode->media as $media)
        <a href="{{$media->url}}">{{$media->type}}</a> - {{$media->human_size}} - {{$media->human_length}}
      @endforeach
    </ul>
  @endif

</div>

@endsection
