<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:admin="http://webns.net/mvcb/" xmlns:atom="http://www.w3.org/2005/Atom/" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" version="2.0">
    <channel>
        <title>Master - {{config('nexus.title.title')}}</title>
        <link>{{route('home')}}</link>
        <pubDate></pubDate>
        <description>This is the master feed for {{config('nexus.title.title')}} that contains all published episodes across all series.</description>
        <language>en-us</language>
        <itunes:subtitle>Master - {{config('nexus.title.title')}}</itunes:subtitle>
        <itunes:author>{{config('nexus.title.title')}}</itunes:author>
        <itunes:summary>This is the master feed for {{config('nexus.title.title')}} that contains all published episodes across all series.</itunes:summary>
        <itunes:image href="???" />
        <itunes:keywords>the nexus, podcast, technological convergence, ryan rampersad</itunes:keywords>
        <itunes:explicit>no</itunes:explicit>
        <itunes:owner>
            <itunes:name>The Nexus</itunes:name>
            <itunes:email>thenexustv@gmail.com</itunes:email>
        </itunes:owner>
        <itunes:category text="Technology">
            <itunes:category text="" />
            <itunes:category text="" />
        </itunes:category>

        @foreach($episodes as $episode)

          <item>
              {{-- <title>{{$episode->number}}: {{$episode->title}}</title> --}}
              <title>{{$episode->formal_title}}</title>
              <link>{{route('episode', [$episode->series->slug, $episode->number])}}</link>
              <guid isPermalink="false">{{route('episode', [$episode->series->slug, $episode->number])}}</guid>
              @if($episode->published_at)
              <pubDate>{{$episode->published_at->format('D, d M Y H:i:s T')}}</pubDate>
              @else
              <pubDate>{{$episode->created_at->format('D, d M Y H:i:s T')}}</pubDate>
              @endif

              <author>???</author>
              <description>{{$episode->description}}</description>

              @if($episode->mp3())
                <enclosure url="{{$episode->mp3()->url}}" length="{{$episode->mp3()->size}}" type="{{$episode->mp3()->mime}}" />
              @else
              <!-- unknown enclosure -->
              @endif

              <content:encoded>
                {{$episode->content}}
              </content:encoded>
              <itunes:author>The Nexus</itunes:author>
              <itunes:duration></itunes:duration>
              <itunes:subtitle>{{$episode->subtitle}}</itunes:subtitle>
              <itunes:summary>{{$episode->description}}</itunes:summary>
              <itunes:keywords>{{$episode->series->meta->itunes_keywords}}</itunes:keywords>
              {{-- <itunes:image href="{{$series->meta->itunes_image_url}}" /> --}}
          </item>

        @endforeach

    </channel>
</rss>
