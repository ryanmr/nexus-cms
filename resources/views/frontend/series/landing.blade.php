@extends('frontend.layout')

@section('content')

<div>

  <div>

    <h2>{{ $series->name }}</h2>

    <blockquote>
      <p>
        {{$series->description}}
      </p>
    </blockquote>

    @if($series->hosts)
    <h3>Hosted By</h3>
    <ul>
      @foreach($series->hosts as $host)
        <li>{{$host->name}}</li>
      @endforeach
    </ul>
    @endif

    <hr />

    @foreach($series->episodes as $episode)
      <h3>
        <a href="{{route('episode', ['series_slug' => $series->slug, 'episode_number' => $episode->number])}}">
          {{$episode->formal_title}}
        </a>
      </h3>
      <p>{{ $episode->description }}</p>
    @endforeach

  </div>

</div>

@endsection
