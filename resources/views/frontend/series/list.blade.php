@extends('frontend.layout')

@section('content')

<div>

  @foreach($series as $serie)
    <div>
      <h3>
        <a href="{{route('series', ['series_slug' => $serie->slug])}}">
          {{$serie->name}}
        </a>
      </h3>
      <blockquote>
        <p>
          {{$serie->description}}
        </p>
      </blockquote>
    </div>
  @endforeach

</div>

@endsection
