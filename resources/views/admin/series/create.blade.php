@extends('admin.admin')

@section('content')

<div class="row">
	{!! Form::model($series, ['method' => 'POST', 'action' => ['Admin\SeriesController@store']]) !!}

	@include ('admin.series._form')

	{!! Form::close() !!}
</div>


@endsection
