@extends('admin.admin')

@section('content')

<div class="row">
	{!! Form::model($series, ['method' => 'PATCH', 'action' => ['Admin\SeriesController@update', $series->id]]) !!}

	@include ('admin.series._form')

	{!! Form::close() !!}
</div>


@endsection
