
<div class="columns large-18">

	<!-- Series Core -->
	<div class="panel">
		<div class="row">

		<div class="columns large-24">
			<h3>Series Core</h3>
		</div>

		<div class="columns large-24">
			<label>Series Name
				{!! Form::text('name', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>Series Slug
				{!! Form::text('slug', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>Series Long Slug
				{!! Form::text('long_slug', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>Series Description
				{!! Form::textarea('description', null) !!}
			</label>
		</div>

		<div class="columns large-8 text-center">
			<label>Series Hidden
				{!! Form::checkbox('hidden') !!}
			</label>
		</div>
		<div class="columns large-8 text-center">
			<label>Series Retired
				{!! Form::checkbox('retired') !!}
			</label>
		</div>
		<div class="columns large-8 text-center">
			<label>Series Hiatus
				{!! Form::checkbox('hiatus') !!}
			</label>
		</div>

		</div>

	</div>

	<!-- Series Defaults, including art and people? -->
	<div class="panel">
		<div class="row">

		<div class="columns large-24">
			<h3>Series Album Art</h3> {{-- fart[album_art_id] --}}
			{!! Form::select('art_list[]',
				 [null => 'None'] + \App\AlbumArt::getArtList()->lists('name', 'id')->toArray(), null)
			!!}
			<p>This will be the default album art for episodes in this series. Changing this after an episode is saved will not change that episode's art.</p>
		</div>

		</div>
	</div>

	<div class="panel">
		<div class="row">

			<div class="columns large-24">
				<h3>Series Hosts</h3>
				{!! Form::select('people_default[]',
					 \App\Person::getPeopleList()->lists('name', 'id')->toArray(), null, ['multiple'])
				!!}
				<p>Select the default hosts for this series. Changing this after an episode is saved will not change that episode's party.</p>
			</div>

		</div>
	</div>

	<!-- Series Meta -->
	<div class="panel">
		<div class="row">

		<div class="columns large-24">
			<h3>Series Meta</h3>
		</div>

		<fieldset>
			<legend>Content</legend>

			<div class="columns large-24">
				<label>Landing Content
					{!! Form::text('meta[landing_content]') !!}
				</label>
				<p class="help-text">Add additional landing page (e.g. ~/series/) specific content.</p>
			</div>

			<div class="columns large-24">
				<label>Attribution Content
					{!! Form::text('meta[attribution_content]') !!}
				</label>
				<p class="help-text">Add additional global attribution content, useful for describing assets used for a series.</p>
			</div>

		</fieldset>

		<fieldset>
			<legend>Feed</legend>

		<div class="columns large-24">
			<label>Feed Author
				{!! Form::text('meta[feed_author]', null) !!}
			</label>
			{{-- <p class="help-text">Usually, <em>The Nexus</em>.</p> --}}
		</div>

		<div class="columns large-24">
			<label>Geographic Location
				{!! Form::text('meta[feed_geographic_location]', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>Episode Frequency
				{!! Form::text('meta[feed_episode_frequency]', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>Feed Tracking URL
				{!! Form::text('meta[feed_tracking_url]', null) !!}
			</label>
			<p class="help-text">Usually this is a link to a Podtrac or similar service's URL that ends in <em>mp3</em>.</p>
		</div>

		<div class="columns large-24">
			<label>Feed Image URL
				{!! Form::text('meta[feed_image_url]', null) !!}
			</label>
			<p class="help-text">A generic large album art image, usually hosted on a CDN.</p>
		</div>

		</fieldset>

		<fieldset>
			<legend>Google Play</legend>

			<div class="columns large-24">
				<label>Feed Tracking URL
					{!! Form::text('meta[google_play_subscription_url]', null) !!}
				</label>
			</div>


		</fieldset>

		<fieldset>
			<legend>iTunes</legend>

		<div class="columns large-24">
			<label>iTunes Subscription URL
				{!! Form::text('meta[itunes_subscription_url]', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>iTunes Summary
				{!! Form::textarea('meta[itunes_summary]', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>iTunes Keywords
				{!! Form::text('meta[itunes_keywords]', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>iTunes Category Primary
				{!! Form::text('meta[itunes_category_primary]', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>iTunes Category Secondary
				{!! Form::text('meta[itunes_category_secondary]', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>iTunes Email
				{!! Form::text('meta[itunes_email]', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>iTunes Explicit
				{!! Form::checkbox('meta[itunes_explicit]', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>iTunes Image URL
				{!! Form::checkbox('meta[itunes_image_url]', null) !!}
			</label>
			<p class="help-text">A large generic album art image, usually hosted on a CDN, meeting the iTunes specifications for album art.</p>
		</div>

		</fieldset>

		</div>
	</div>

</div><!-- end column for series -->


<div class="columns large-6">
	<div class="panel">
		{!! Form::submit('Save', ['class' => 'button expand']) !!}
	</div>
</div>
