@extends('admin.admin')

@section('content')

<div class="row">

<div class="page-navigation">
	<dl class="sub-nav">
	  <dt>Series:</dt>
	  <dd><a href="/admin/series/create">New Series</a></dd>
	</dl>
</div>

<div class="columns large-24">
		<table>
			<thead>
				<tr>
					<th>Series</th>
					<th>Slug</th>
					<th>Description</th>
					<th>Created</th>
				</tr>
			</thead>

			<tbody>
				@foreach ($series as $serie)
				<tr>
					<td class="text-center"><span class="title" title="{{ $serie->id }}">{{ $serie->name }}</span></td>
					<td class="text-center">
						<span class="short slug">{{$serie->slug}}</span>
						<br />
						<span title="long slug">{{$serie->long_slug}}</span>
					</td>
					<td>
						<p>{{ $serie->description }}</p>
						<ul class="button-group list-actions">

							<li><a class="button tiny secondary" href="{{ route('series', $serie->slug) }}">View</a></li>
							<li><a class="button tiny secondary" href="{{ route('admin.series.edit', $serie->id) }}">Edit</a></li>
							<li>
								{{-- <a class="button tiny warning" data-delete="true" href="{{ route('admin.series.destroy', $serie->id) }}">Delete</a> --}}
							</li>

						</ul>
					</td>
					<td>{{ $serie->created_at->format('F j, Y') }}</td>
				</tr>
				@endforeach
			</tbody>

		</table>
</div>

</div>


@endsection
