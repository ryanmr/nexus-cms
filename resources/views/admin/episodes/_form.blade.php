
	<div class="columns large-18">

		<div class="panel">
			<div class="row">

				<div class="columns large-24">
					<h3>Episode</h3>
				</div>

				<div class="columns large-24">
					<label>Title:
						<input type="text" v-model="episode.title">
					</label>
				</div>

				<div class="columns large-24" v-show="episode.title">
					<label>Subtitle:
						<input type="text" v-model="episode.subtitle">
					</label>
				</div>

				<div class="columns large-24">
					<label>Series:
						<select class="form-control"
						v-model="episode.series_id"
						>
							<option value="">Select a series</option>
							<option v-for="s in series" v-bind:value="s.value">@{{s.text}}</option>
						</select>
					</label>
				</div>

				<div class="columns large-24">
					<label>Number:
						<input type="number" v-model="episode.number">
					</label>
				</div>

				<div class="columns large-24">
					<label>Content:
						<textarea v-model="episode.content" rows="12" cols="40"></textarea>
					</label>
				</div>

				<div class="columns large-24">
					<label>Description:
						<textarea v-model="episode.description" rows="3" cols="40"></textarea>
					</label>
					<p class="help-text">Try to limit the episode description to 160 characters.</p>
				</div>

				<div class="columns medium-8 text-center">
					<label>Hidden
						<input type="checkbox" v-model="episode.hidden">
					</label>
					<p class="help-text">When an episode is <em>hidden</em>, it will not be visible or accessible from the front end.</p>
				</div>
				<div class="columns medium-8 text-center">
					<label>NSFW
						<input type="checkbox" v-model="episode.nsfw">
					</label>
					<p class="help-text">When an episode is <em>NSFW</em>, it will be shown as such on its episode page.</p>
				</div>
				<div class="columns medium-8 text-center">
					<label>Unlisted
						<input type="checkbox" v-model="episode.unlisted">
					</label>
					<p class="help-text">
						When an episode is <em>unlisted</em>, it is not listed on the front end, but is still directly accessible.
					</p>
				</div>

			</div>
		</div>

		<div class="panel">
			<div class="row">

				<div class="columns medium-24 text-center">
					<label>Debug
						<input type="checkbox" v-model="meta.debug">
					</label>
					<p class="help-text">Do you want to temporarily see the debug?</p>
				</div>

<div class="columns large-24" v-show="meta.debug">
<pre>
@{{ episode | json }}
</pre>
</div>

			</div>
		</div>

		<div class="panel">
			<div class="row">

			<div class="columns large-24">
				<h3>People</h3>

				<label>Add A Person</label>
				<input class="form-control" type="text" placeholder="Search for a person"
				v-el:peoplequery
				v-model="queries.people"
				>
				<ul class="no-bullet" v-if="episode.people.length > 0">
					<li class="list-group-item" v-for="person in episode.people" track-by="id">
				@{{person.name}}
						<select v-model="person.pivot.role">
							<option v-for="p in peopleRoles" v-bind:value="p.value">@{{p.text}}</option>
						</select>
						<button v-on:click="removePerson(person)" type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</li>
				</ul>
				<p class="help-text">If left blank, the default series hosts will be automatically added to this episode.</p>

			</div>

			</div>
		</div><!-- episode.people -->

		<div class="panel">
			<div class="row">

				<div class="columns large-24">

				<h4>Related</h4>

        <div class="form-group">
          <label>Parent:</label>
          <input class="form-control" type="text" placeholder="Select the parent episode"
            v-el:relatedparent
            v-model="queries.related.parent"
            v-bind:disabled="parents.length > 0"
          >
          <div v-if="parents.length > 0">
            <div v-for="parent in parents">
              @{{parent.name}} <button v-on:click="removeRelated(parent)" type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label>Fringe:</label>
          <input class="form-control" type="text" placeholder="Select the fringe episode"
            v-el:relatedfringe
            v-model="queries.related.fringe"
            v-bind:disabled="fringes.length > 0"
          >
          <div v-if="fringes.length > 0">
            <div v-for="fringe in fringes">
              @{{fringe.name}} <button v-on:click="removeRelated(fringe)" type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
          </div>
        </div>

				</div>

			</div>
		</div><!-- episode.related -->

		<div class="panel">
			<div class="row">

				<div class="columns large-24">

					<h4>Media</h4>
					<div class="form-group">
						<a class="button" href="#" role="button"
							v-on:click="newMP3Media"
						>New MP3 Media</a>
					</div>

						<div class="row" v-for="media in episode.media">
							<div class="columns large-24">

								<label>@{{media.type | uppercase}}:</label>

								<div>
									<input type="text" v-model="media.url">
									<button type="button" class="btn btn-primary"
										v-on:click="checkMedia(media, $event)"
									>Update</button>
									<button v-on:click="removeMedia(media, $event)" type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								</div>

								<div v-if="media.length && media.size">
									@{{media.length}} | @{{media.size}}
								</div>

							</div>
						</div>

				</div>

			</div>
		</div>

		<div class="panel">
			<div class="row">
				<div class="columns large-24">

					<h4>Album Art</h4>

					<select v-model="episode.album_art_id">
						<option :value="">Default</option>
						<option v-for="a in art" :value="a.value">@{{a.text}}</option>
					</select>
					<p class="help-text">Select specific album art for this episode, otherwise the default series album art will be used.</p>
				</div>
			</div>
		</div>

		<div class="panel">
			<div class="row">

				<div class="columns large-24">
					<h4>Other</h4>

					<label>Episode Notes:
						<textarea v-model="episode.notes" rows="5" cols="40"></textarea>
					</label>
					<p class="help-text">Private notes on this episode.</p>
				</div>

			</div>
		</div>

	</div><!-- all episode modules -->

	<div class="columns large-6">
		<div class="panel">

			<label>Episode State
				<select class="form-control"
				v-model="episode.state"
				>
					<option v-for="s in states" :value="s.value">@{{s.text}}</option>
				</select>
			</label>

			<input class="button expand" type="submit" :disabled="saveable == false" value="Save">

		</div>
	</div>
