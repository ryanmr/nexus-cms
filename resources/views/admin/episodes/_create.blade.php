@extends('admin.admin')

@section('content')

<div class="row">
	{!! Form::open(['action' => ['Admin\EpisodeController@store']]) !!}

	@include ('admin.episodes._form')

	{!! Form::close() !!}
</div>


@endsection
