<div class="media">

	<div class="columns large-18">
		<label>
			Media Type
			{!! Form::select("media[{$media->id}][type]", array_map(
			function($i){return strtoupper($i);},
			Config::get('nexus.media.types')),
			$media->type
			)
	!!}
		</label>
	</div>

	<div class="columns large-24">
		<label>Media URL
			<div class="row collapse">
				<div class="small-20 columns">
					{!! Form::text("media[{$media->id}][url]", $media->url, ['class' => 'form-media-url']) !!}
				</div>
				<div class="small-4 columns">
					<a href="#" class="form-media-load button postfix action-load">Load</a>
				</div>
			</div>
		</label>
	</div>

	<div class="columns large-24">
		<ul class="inline-list hide">
			<li>
				Length: <span class="form-media-length-display"></span>
				<input class="form-media-length" type="hidden" />
			</li>
			<li>
				Size: <span class="form-media-size-display"></span>
			</li>
			<li>
				Type: <span class="form-media-type-display"></span>
			</li>
			<li>
				MIME: <span class="form-media-mime-display"></span>
			</li>
			<li>

			</li>
		</ul>
	</div>

	<div class="columns large-24">
		<button class="action-delete button tiny secondary right">&times;</button>
	</div>


</div>
