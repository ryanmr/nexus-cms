@extends('admin.admin')

@section('content')

<div class="row">

<div class="page-navigation">
	<dl class="sub-nav">
	  <dt>Episodes:</dt>
	  <dd><a href="/admin/episodes/create">New Episode</a></dd>
	</dl>
</div>

<div class="columns large-24">

	<div class="pagination-centered">
		{!! $episodes->render(); !!}
	</div>

	<table class="full">
		<thead>
			<tr>
				<th>Title</th>
				<th>Series</th>
				<th>Created</th>
				<th>Status</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($episodes as $episode)
			<tr>
				<td>
					<span class="title" title="{{ $episode->id }}"><a href="{{ route('admin.episodes.edit', $episode->id) }}">{{ $episode->formal_title }}</a></span>
						<ul class="button-group list-actions">

						<li><a class="button tiny secondary" href="{{ route('admin.episodes.edit', $episode->id) }}">Edit</a></li>
						<li><a class="button tiny secondary" href="{{ route('episode', [$episode->series->slug, $episode->number]) }}">View</a></li>
						<li>
							<a class="button tiny warning" data-delete="true" href="{{ route('admin.episodes.destroy', $episode->id) }}">Delete</a>
							{{-- delete button supported by javascript --}}
						</li>

					</ul>
				</td>
				<td class="text-center">{{ $episode->series->name }}</td>
				<td class="text-center">{{ $episode->created_at->format('F j, Y') }}</td>
				<td class="text-center">{{ ucfirst($episode->state) }}</td>
			</tr>
			@endforeach
		</tbody>

	</table>

	<div class="pagination-centered">
		{!! $episodes->render(); !!}
	</div>

</div>


</div>


@endsection
