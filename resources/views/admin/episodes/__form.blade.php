
<div class="columns large-18">

	<div class="panel">
		<div class="row">

		<div class="columns large-24">
			<h3>Episode</h3>
		</div>

		<div class="columns large-24">
			<label>Episode Name
				{!! Form::text('name', null) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>Episode Series
				{!! Form::select('series_id', $series) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>Episode Number
				{!! Form::input('number', 'number') !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>Episode Content
				{!! Form::textarea('content', null, ['rows' => 20, 'class' => 'content-field']) !!}
			</label>
		</div>

		<div class="columns large-24">
			<label>Episode Description
				{!! Form::textarea('description', null, ['rows' => 4]) !!}
			</label>
		</div>


		<div class="columns large-12 text-center">
			<label>Hidden
				{!! Form::checkbox('hidden') !!}
			</label>
		</div>
		<div class="columns large-12 text-center">
			<label>NSFW
				{!! Form::checkbox('nsfw') !!}
			</label>
		</div>


		</div>
	</div>

	<div class="panel">
		<div class="row">

		<div class="columns large-24">
			<h3>People</h3>

			<div class="people-container" data-id="{{ $episode->id or -1 }}"></div>

		</div>

		</div>
	</div>

	<div class="panel">
		<div class="row">

			<div class="columns large-24">
				<h3>Related</h3>

				<div class="related-container" data-id="{{ $episode->id or -1 }}"></div>

			</div>

		</div>
	</div>

	<div class="panel">
		<div class="row">

		<div class="columns large-24">
			<h3>Media</h3>

			<div class="media-container" data-id="{{ $episode->id or -1 }}">

			</div>

		</div>

		</div>
	</div>

	<div class="panel">
		<div class="row">

		<div class="columns large-24">
			<h3>Art</h3>

			{!! Form::select('art[album_art_id]',
				   [null => 'Series Default'] + \App\AlbumArt::getArtList()->lists('name', 'id')->toArray(),
				   isset($episode) ? $episode->art->lists('id')->toArray() : [])
			!!}


		</div>

		</div>
	</div>

</div>


<div class="columns large-6">
	<div class="panel">

		{{-- <div class="columns large-24"> --}}
			<label>Episode State
				{!! Form::select('state', array_map(
						function($i){return ucfirst($i);},
						Config::get('nexus.episode.states'))
						)
				!!}
			</label>
		{{-- </div> --}}

		{!! Form::submit('Save', ['class' => 'button expand']) !!}


	</div>
</div>
