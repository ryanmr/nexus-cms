@extends('admin.admin')

@section('content')

<div class="row">
	{!! Form::model($episode, ['method' => 'PATCH', 'action' => ['Admin\EpisodeController@update', $episode->id]]) !!}

	@include ('admin.episodes._form')

	{!! Form::close() !!}
</div>


@endsection
