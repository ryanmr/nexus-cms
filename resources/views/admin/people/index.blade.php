@extends('admin.admin')

@section('content')


<div class="row">

<div class="page-navigation">
	<dl class="sub-nav">
	  <dt>Episodes:</dt>
	  <dd><a href="/admin/people/create">New Person</a></dd>
	</dl>
</div>

<div class="columns large-24">

	<table class="full">
		<thead>
			<tr>
				<th>Name</th>
				<th>Created</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($people as $person)
			<tr>
				<td>
					<span class="name" title="{{ $person->id }}"><a href="{{ route('admin.people.edit', $person->id) }}">{{ $person->name }}</a></span>

					<ul class="button-group list-actions">

					<li><a class="button tiny secondary" href="{{ route('admin.people.edit', $person->id) }}">Edit</a></li>
					<li>
						<a class="button tiny warning" data-delete="true" href="{{ route('admin.people.destroy', $person->id) }}">Delete</a>
						{{-- delete button supported by javascript --}}
					</li>

				</ul>

				</td>
				<td class="text-center">{{ $person->created_at->format('F j, Y') }}</td>
			</tr>
			@endforeach
		</tbody>

	</table>


</div>


</div>


@endsection
