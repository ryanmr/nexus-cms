@extends('admin.admin')

@section('content')

<div class="row">
	{!! Form::open(['action' => ['Admin\PeopleController@store']]) !!}

	@include ('admin.people._form')

	{!! Form::close() !!}
</div>


@endsection
