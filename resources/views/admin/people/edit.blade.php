@extends('admin.admin')

@section('content')

<div class="row">
	{!! Form::model($person, ['method' => 'PATCH', 'action' => ['Admin\PeopleController@update', $person->id]]) !!}

	@include ('admin.people._form')

	{!! Form::close() !!}
</div>


@endsection
