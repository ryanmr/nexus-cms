
<div class="columns large-18">

	<!-- Series Core -->
	<div class="panel">
		<div class="row">

			<div class="columns large-24">
				<h3>Person</h3>
			</div>

			<div class="columns large-24">
				<label>Person Name
					{!! Form::text('name') !!}
				</label>
			</div>

			<div class="columns large-24">
				<label>Person Slug
					{!! Form::text('slug') !!}
				</label>
			</div>

			<div class="columns large-24">
				<label>Person Email
					{!! Form::email('email') !!}
				</label>
			</div>

			<div class="columns large-24">
				<label>Person Description
					{!! Form::textarea('content') !!}
				</label>
			</div>

			<div class="columns large-24 text-center">
				<label>Person Hidden
					{!! Form::checkbox('hidden') !!}
				</label>
			</div>

			<div class="columns large-24">
				<label>Web URL
					{!! Form::text('web_url') !!}
				</label>
			</div>

			<div class="columns large-24">
				<label>Twitter URL
					{!! Form::text('twitter_url') !!}
				</label>
			</div>

			<div class="columns large-24">
				<label>Social URL
					{!! Form::text('social_url') !!}
				</label>
			</div>

		</div>
	</div>

</div><!-- end column for series -->


<div class="columns large-6">
	<div class="panel">
		{!! Form::submit('Save', ['class' => 'button expand']) !!}
	</div>
</div>
