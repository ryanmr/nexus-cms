@extends('admin.admin')

@section('content')

<div class="row">
	{!! Form::model($art, ['method' => 'PATCH', 'action' => ['Admin\ArtController@update', $art->id]]) !!}

		<div class="columns large-12">
			<div class="panel">
				<div class="row">

					<div class="columns large-24">
						<label>Art Name
							{!! Form::text('name') !!}
						</label>
					</div>

				</div>
			</div>
		</div>

		<div class="columns large-12">
			<div class="panel">

				{!! Form::submit('Save', ['class' => 'button expand']) !!}

				<div class="dates">
					<p>
						<label class="created">Uploaded On:
							<time datetime="{{ $art->created_at->format('l jS \of F Y h:i:s A') }}">{{ $art->created_at->format('l, F jS, Y') }}</time>
						</label>
						@if ($art->created_at != $art->updated_at)
							<label class="updated">Updated On:
								<time datetime="{{ $art->updated_at->format('l jS \of F Y h:i:s A') }}">{{ $art->updated_at->format('l, F jS, Y') }}</time>
							</label>
						@endif
					</p>
				</div>

			</div>
		</div>

		<div class="columns large-12 end">
			<div class="panel">
				<img src="{{ $art->getView('medium')->getViewablePath() }}" class="album-art-image" />
			</div>
		</div>

	{!! Form::close() !!}
</div>


@endsection
