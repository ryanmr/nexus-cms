
<div class="columns large-24">

	<div class="panel">
		<div class="row">

			<div class="columns large-24">
				<h3>Upload Art</h3>

				<p>After uploading the art files, they can be named.</p>

			</div>

			<div class="columns large-24">
				<form id="artUpload" class="dropzone" action="{{ action('Admin\ArtController@upload') }}" method="post">
					{{ csrf_field() }}
				</form>
			</div>

		</div>
	</div>



</div>
