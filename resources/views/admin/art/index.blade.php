@extends('admin.admin')

@section('content')

<div class="row">

<div class="page-navigation">
	<dl class="sub-nav">
	  <dt>Art:</dt>
	  <dd><a href="/admin/art/create">New Art</a></dd>
	</dl>
</div>

<div class="columns large-24">

  @foreach ($arts->chunk(3) as $row)
    <div class="row gallery-row">

      @foreach ($row as $art)
        <div class="columns small-12 medium-8">
          <div class="gallery-item">
						<a href="{!! route('admin.art.edit', $art->id) !!}">
							<img src="{{ $art->getView()->getViewablePath() }}"  />
							<div>
								<span>{{ $art->name }} <time>{{ $art->created_at->format('Y-m-d') }}</time></span>
							</div>
						</a>
          </div>
				</div>
      @endforeach

    </div>
  @endforeach

</div>

<div class="pagination-centered">
</div>

</div>


@endsection
