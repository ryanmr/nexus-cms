@inject('recents', 'App\Components\Recents')

<div class="columns large-12 medium-12">
	<div class="box">
		<div class="recently-published">

			<div class="box-title">
				<h3>Recently</h3>
			</div>

			<div class="box-inner">
				<div class="episodes">
					@foreach ($recents->getRecentlyPublishedEpisodes() as $episode)
						<div class="episode">
							<h3><a href="{{ route('episode', [$episode->series->slug, $episode->number]) }}">{{ $episode->formal_title }}</a></h3>
							<p>{{ $episode->description }}</p>
							<span class="when">{{ $episode->created_at->diffForHumans() }}</span>
						</div>
					@endforeach
				</div>
			</div>

		</div>
	</div>
</div>
