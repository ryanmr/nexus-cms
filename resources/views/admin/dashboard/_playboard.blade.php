@inject('playboard', 'App\Components\Playboard')

<div class="columns large-12 medium-12">
	<div class="box">
		<div class="playboard">

			<div class="box-title">
				<h3>Playboard</h3>
			</div>

			<div class="box-inner">
				<dl>
					<div class="row">
					@foreach ($playboard->getSeries() as $sc)
						<div class="columns large-8 medium-12 small-12">
							<dt>{{ strtoupper($sc->slug) }}</dt>
							<dd>{{ $sc->episodes->count() }}</dd>
						</div>
					@endforeach
					</div>
				</dl>

				<hr />

				<dl>
					<div class="row">
						<div class="columns large-6 small-12">
							<dt>Total</dt>
							<dd>{{ $playboard->getPeriodCount('total') }}</dd>
						</div>
						<div class="columns large-6 small-12">
							<dt>90 Days</dt>
							<dd>{{ $playboard->getPeriodCount('quaterly') }}</dd>
						</div>
						<div class="columns large-6 small-12">
							<dt>30 Days</dt>
							<dd>{{ $playboard->getPeriodCount('monthly') }}</dd>
						</div>
						<div class="columns large-6 small-12">
							<dt>7 Days</dt>
							<dd>{{ $playboard->getPeriodCount('weekly') }}</dd>
						</div>
					</div>
				</dl>

			</div>

		</div>
	</div>
</div>
