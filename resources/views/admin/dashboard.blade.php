@extends('admin.admin')

@section('content')

  <div class="row">

    @include('admin/dashboard/_playboard')

    @include('admin/dashboard/_recently-published')

  </div>

@endsection
