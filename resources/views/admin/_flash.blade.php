
@if (Session::has('flash.message'))
	<div class="row">
		<div class="columns large-18 large-centered">
			<div data-alert class="alert-box {{ Session::has('flash.level') ? Session::get('flash.level') : 'secondary' }}">
				{{ Session::get('flash.message') }}
				<a href="#" class="close" title="Close">&times;</a>
			</div>
		</div>
	</div>
@endif