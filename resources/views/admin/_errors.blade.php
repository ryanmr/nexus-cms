@if ($errors->has())
<div class="row">
	<div class="columns large-18 large-centered">
		<div class="alert-box warning">
			<ul>
		    @foreach ($errors->all() as $error)
		        <li>{{ $error }}</li>
		    @endforeach
			</ul>
		</div>
	</div>
</div>
@endif