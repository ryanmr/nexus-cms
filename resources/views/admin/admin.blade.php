<!doctype html>
<html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{config('nexus.title.title')}}</title>

	<meta name="_token" content="{{ csrf_token() }}" />

	<link rel="stylesheet" href="/css/build/vendor.css">
	<link rel="stylesheet" href="/css/build/admin.css">

</head>
<body id="app">

<nav class="top-bar" data-topbar role="navigation">
  <ul class="title-area">
    <li class="name">
      <h1><a href="/">The Nexus</a></h1>
    </li>

  </ul>

  <section class="top-bar-section">
    <!-- Right Nav Section -->
    <ul class="right">

    	@if (Auth::guest())
			<li><a href="/auth/login">Login</a></li>
			<li><a href="/auth/register">Register</a></li>
    	@else
			<li class="has-dropdown">
			<a href="#">{{ Auth::user()->name }}</a>
				<ul class="dropdown">
					<li><a href="/auth/logout">Logout</a></li>
				</ul>
			</li>
    	@endif
    </ul>

    <!-- Left Nav Section -->
    <ul class="left">

	    <li><a href="/admin/dashboard">Dashboard</a></li>

			<li class="has-dropdown">
			<a href="/admin/series">Series</a>
				<ul class="dropdown">
					<li><a href="/admin/series/create">New Series</a></li>
				</ul>
			</li>

			<li class="has-dropdown">
			<a href="/admin/episodes">Episodes</a>
				<ul class="dropdown">
					<li><a href="/admin/episodes/create">New Episode</a></li>
				</ul>
			</li>

			<li class="has-dropdown">
			<a href="/admin/people">People</a>
				<ul class="dropdown">
					<li><a href="/admin/people/create">New Person</a></li>
				</ul>
			</li>

			<li class="has-dropdown">
			<a href="/admin/art">Art</a>
				<ul class="dropdown">
					<li><a href="/admin/art/create">New Art</a></li>
				</ul>
			</li>

    </ul>
  </section>
</nav>

	<!-- hax -->
	<div class="row">
	<p></p>
	</div>

	@include('admin/_flash')

	@include('admin/_errors')

	@yield('content')

	<script type="text/javascript" src="/js/build/vendor.js"></script>
	<script type="text/javascript" src="/js/build/admin.js"></script>
	<script type="text/javascript" src="/js/build/components.js"></script>

	<script type="text/javascript">
		$(document).foundation();
	</script>

	@yield('scripts.footer')

</body>
</html>
