<!doctype html>
<html>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Authentication</title>

	<link rel="stylesheet" href="/css/build/vendor.css">
	<link rel="stylesheet" href="/css/build/admin.css">

	<script type="text/javascript" src="/js/build/vendor.js"></script>
	<script type="text/javascript" src="/js/build/admin.js"></script>

</head>
<body>

<nav class="top-bar" data-topbar role="navigation">
  <ul class="title-area">
    <li class="name">
      <h1><a href="/">The Nexus</a></h1>
    </li>

  </ul>

  <section class="top-bar-section">
    <!-- Right Nav Section -->
    <ul class="right">

    	@if (Auth::guest())
			<li><a href="/auth/login">Login</a></li>
			<li><a href="/auth/register">Register</a></li>
    	@else
			<li class="has-dropdown">
			<a href="#">{{ Auth::user()->name }}</a>
				<ul class="dropdown">
					<li><a href="/auth/logout">Lougout</a></li>
				</ul>
			</li>
    	@endif
    </ul>

    <!-- Left Nav Section -->
    <ul class="left">
      <li><a href="/admin/dashboard">Dashboard</a></li>
    </ul>
  </section>
</nav>

	@yield('content')


	<script type="text/javascript">
		$(document).foundation();
	</script>

	@yield('scripts.footer')

</body>
</html>
