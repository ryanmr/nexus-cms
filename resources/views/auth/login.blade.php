@extends('auth/auth')

@section('content')
<div class="row">

	<div class="columns large-24">
		<div class="panel">

			<form class="form-horizontal" role="form" method="POST" action="/login">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="form-group">
					<label class=" control-label">E-Mail Address</label>
					<div class="">
						<input type="email" class="form-control" name="email" value="{{ old('email') }}">
					</div>
				</div>

				<div class="form-group">
					<label class=" control-label">Password</label>
					<div class="">
						<input type="password" class="form-control" name="password">
					</div>
				</div>

				<div class="form-group">
					<div class="">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="remember"> Remember Me
							</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="">
						<button type="submit" class="btn btn-primary" style="margin-right: 15px;">
							Login
						</button>

						<a href="/password/email">Forgot Your Password?</a>
					</div>
				</div>
			</form>

		</div>
	</div>

</div>
@endsection
