var PeopleSearch = React.createClass({
	getInitialState: function() {
		return {text: ''};
	},

	componentDidMount: function() {
		var element = React.findDOMNode(this.refs.input);
		$(element).autocomplete({
			source: '/admin/ajax/people/search/',
			select: this.select
		});
	},

	select: function(event, data) {
		console.log('autocomplete selected');
		this.props.onSelected(data); // hacks!
		this.setState({text: ''})
		console.log(this.state.text);

		/*
			Prevent the textbox from filling with the ID of the selection;
			http://salman-w.blogspot.com/2013/12/jquery-ui-autocomplete-examples.html#example-1
		*/
		event.preventDefault();
	},

	componentWillUnmount: function() {
		var element = React.findDOMNode(this.refs.input);
		$(element).autocomplete('destroy');
	},

	onType: function(event) {
		this.setState({text: event.target.value});
	},

	render: function() {
		console.log("episode people search rendered");
		return (
			<div>
				<input placeholder="Search for people" ref="input" onChange={this.onType} value={this.state.text} type="text" />
			</div>
		);
	}
});

var PersonListItem = React.createClass({

// tip: http://javaguirre.net/2014/02/09/reactjs-by-example/
delete: function(event) {
	event.preventDefault();
	this.props.onDelete(this.props.person);
},

render: function() {
	console.log("PersonListItem rendered");
	var name_id = 'people['+this.props.person.id+'][id]';
	var name_role = 'people['+this.props.person.id+'][role]';
	var id = this.props.person.id;

	// used to be global variable; localized for now
	var people_roles = [
		'host',
		'guest'
	];

	var options = people_roles.map(function(role){
		return <option key={role} value={role}>{ toTitleCase(role) }</option>
	});

	return (
		<li>
			{this.props.person.name} <a href="#" person={this.props.person} onClick={this.delete} ref="delete" title="Delete" className="delete">&times;</a>
			<select name={name_role} defaultValue={this.props.person.role}>
				{options}
			</select>


		</li>
	);

	// <input type="hidden" name={name_id} value={id} />
	// might not be needed?
}
});

var PeopleList = React.createClass({

	render: function() {
		console.log("episode people list rendered");

		var people = this.props.people.map(function(person){
			return <PersonListItem key={person.id} person={person} onDelete={this.props.onDelete} />;
		}.bind(this));

		return (
			<ul className="no-bullet">
				{people}
			</ul>
		);
	}
});

var PeopleBox = React.createClass({

	getInitialState: function() {
		return {
			people: []
		};
	},

	load: function() {
		if (this.props.episode_id == -1) {
			console.log('PeopleBox did not fetch remote data; no episode_id specified');
			return;
		}

		var self = this; // hacks!

		$.ajax({
			url: '/admin/ajax/people/initial/',
			data: {id: this.props.episode_id}
		}).done(function(data){
			console.log('PeopleBox: initial people set: %o', data);
			$.each(data, function(i, d){
				var insert = {name: d.name, id: d.id, role: d.pivot.role};
				self.setState({ people: self.state.people.concat(insert) });
			});
		});

	},

	componentDidMount: function() {
		this.load();
	},

	selected: function(data) {
		console.log('data returned: %o', data);
		if (!data.item) {
			console.log('data returned was invalid');
			return;
		}
		var insert = {name: data.item.label, id: data.item.value};
		this.setState({ people: this.state.people.concat(insert) });
	},

	deleted: function(person) {

		console.log('deleting person:  %o', person);

		var people = this.state.people.filter(function(p) {
          return p.id !== person.id;
      });
		this.setState({people: people});
	},

	render: function() {

		console.log("episode people box rendered");

		return (
			<div>
				<PeopleSearch onSelected={this.selected} />
				<PeopleList people={this.state.people} onDelete={this.deleted} />
			</div>
		);
	}
});
