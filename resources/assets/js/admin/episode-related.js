var RelatedEntry = React.createClass({

	delete: function(event) {
		this.props.onDelete(this.props.type);
		event.preventDefault();
	},

	render: function() {

		console.log(this.props);

		if (this.props.related == null || this.props.related == undefined) {
			console.log('related entry %o did not render, no data supplied', this.props.type);
			return null;
		}

		console.log('related entry %o rendering', this.props.type);

		var name_id = 'related['+this.props.related.id+'][id]';
		var name_type = 'related['+this.props.related.id+'][type]';

		return (
			<p>
				Related {toTitleCase(this.props.type)}: {this.props.related.name} <a href="#" related={this.props.related} onClick={this.delete} ref="delete" title="Delete" className="delete">&times;</a>
				<input type="hidden" name={name_type} value={this.props.type} />
			</p>
		);
	}

});

var RelatedSearchBox = React.createClass({
	getInitialState: function() {
		return {
			text: ''
		};
	},

	componentDidMount: function() {
		console.log('RelatedSearchBox did mount');
		var element = React.findDOMNode(this.refs.input);
		$(element).autocomplete({
			source: '/admin/ajax/episode/related/search/',
			select: this.select
		});
	},

	select: function(event, data) {
		// send back the type accordingly
		this.props.onSelected(this.props.type, data); // hacks!

		this.setState({text: data.name});

		event.preventDefault();
	},

	componentWillUnmount: function() {
		console.log('RelatedSearchBox will unmount');
		var element = React.findDOMNode(this.refs.input);
		$(element).autocomplete('destroy');
	},

	onType: function(event) {
		this.setState({text: event.target.value});
	},

	render: function() {

		var searchtext = "Search for " + (this.props.type == 'fringe' ? 'a fringe episode' : 'an episode');

		console.log('RelatedSearchBox %o: this.props.related is %o', this.props.type, this.props.related);

		var input = <input placeholder={searchtext} ref="input" onChange={this.onType} value={this.state.text} type="text" />;

		var entry = <RelatedEntry related={this.props.related} type={this.props.type} onDelete={this.props.onDelete} />;

		/*
				This will hide the input box whenever there is already a selection being shown.
		*/

		var hide = this.props.related ? 'hide' : '';

		return (
			<div>
				<label>{ toTitleCase(this.props.type) }:</label>
				<div className={hide}>{input}</div>
				{entry}
			</div>
		);
	}
});

var RelatedBox = React.createClass({

	getInitialState: function() {
		return {
			parent: null,
			fringe: null
		};
	},

	load: function() {
		if (this.props.episode_id == -1) {
			console.log('RelatedBox did not fetch remote data; no episode_id specified');
			return;
		}

		var self = this; // hacks!

		$.ajax({
			url: '/admin/ajax/episode/related/initial/',
			data: {id: this.props.episode_id}
		}).done(function(data){
			console.log('RelatedBox: initial people set: %o', data);

			if (!data) {
				console.log('related box: data was invalid');
				return;
			}

			$.each(data, function(i, d){
				/*
					The formal_name attribute is the fancier version of the regular name,
					as such it handles fringes and special things.
				 */
				var f = {name: d.formal_name, id: d.id};
				self.setRelated(d.pivot.type, f);
			});

		});

	},

	setRelated: function(type, data) {
		if (type == 'fringe') {
			this.setState({fringe: data});
		} else {
			this.setState({parent: data});
		}

		console.log('related set: %o', this.state);
	},

	componentDidMount: function() {
		this.load();
	},

	selected: function(type, data) {
		if (!data.item) {
			console.log('related box: data was invalid');
			return;
		}

		var d = {name: data.item.label, id: data.item.value};

		this.setRelated(type, d);
	},

	deleted: function(type) {
		this.setRelated(type, null);
	},

	render: function() {
		return (
			<div>
				<RelatedSearchBox type="parent" onSelected={this.selected} onDelete={this.deleted} related={this.state.parent} />
				<RelatedSearchBox type="fringe" onSelected={this.selected} onDelete={this.deleted} related={this.state.fringe} />
			</div>
		);
	}
});
