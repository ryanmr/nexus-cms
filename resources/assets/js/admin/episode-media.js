var media_types = [
  'mp3'
];

var MediaURL = React.createClass({

  checked: function(event) {

    var url = React.findDOMNode(this.refs.input).value;

    this.props.onChecked(url, this.props.uid);

    event.preventDefault();
  },

  render: function() {

    console.log('MediaURL: rendered');

    var url;
    if (!this.props.url) {
      url = '';
    }

    /*
      This is dependent on Foundation 5 styles.
    */
    return (
      <div className="row collapse">
          <div className="small-20 columns">
            <input type="text" placeholder="MP3 Audio URL" ref="input" defaultValue={this.props.url} />
          </div>
          <div className="small-4 columns">
            <a href="#" className="button postfix" onClick={this.checked}>Check</a>
          </div>
      </div>
    );
  }
});

var MediaInformation = React.createClass({

  remove: function(event) {
    this.props.onRemove(this.props.uid);
    event.preventDefault();
  },

  render: function() {

    /*
      --If the media does not have a specific ID, do not send any data up
      Since ID may not be set (when adding a new item), ensure that at least the size and length are set
    */
    if (this.props.media.size == undefined) {
      console.log('MediaInformation: not rendered');
      return null;
    }

    console.log('MediaInformation: rendered');

    // since this is what gets pushed up to the server,
    // the id is either an actual ID number or a blank array part
    var id;
    if (this.props.media.id) {
      id = this.props.media.id;
    } else {
      id = '';
    }

    var self = this;
    var index = this.props.index;

    var inputs = $.map(this.props.media, function(value, key) {
      var name = 'media['+index+']['+key+']';
      return <input key={key} type="hidden" name={name} value={value} />
    });


    return (
      <div>
        <ul className="inline-list">
          <li><strong>Duration:</strong> {this.props.media.length}</li>
          <li><strong>Filesize:</strong> {this.props.media.size}</li>
          <li><a href="#" onClick={this.remove}>Remove Media</a></li>
        </ul>
        {inputs}
      </div>
    );

  }
});

var MediaBoxError = React.createClass({

  render: function() {
    if (!this.props.message) {
      return null;
    }

    return (
      <div data-alert className="alert-box alert round">
      {this.props.message}
      </div>
    );

  }

});

var MediaBox = React.createClass({

  getInitialState: function() {
      return {
        media: [],
        errorMessage: null
      };
  },

  resetErrors: function() {
    this.setState({errorMessage: null});
  },

  load: function() {
		if (this.props.episode_id == -1) {
			console.log('MediaBox: did not fetch remote data; no episode_id specified');
			return;
		}

		var self = this; // hacks!

		$.ajax({
			url: '/admin/ajax/media/initial/',
			data: {id: this.props.episode_id}
		}).done(function(data){
			console.log('MediaBox: initial data: %o', data);

			if (!data) {
				console.log('MediaBox: data was invalid');
				return;
			}

			$.each(data, function(i, d){
        // d is a media object, basically,
        // so let's add a uid element to it
        d.uid = get_unique_identifier();
        self.setState({media: self.state.media.concat(d)});
			});

		});

	},

  componentDidMount: function() {
    console.log('MediaBox: did mount');
    this.load();
  },

  addNewMedia: function(event) {
    console.log('MediaBox: new media');

    // uses a unique idenitifer to determine children
    var uid = get_unique_identifier();


    var media = {
      uid: uid
    };

    this.setState({media: this.state.media.concat(media)});

    event.preventDefault();
  },

  checked: function(url, uid) {

    this.resetErrors();

    var self = this;
    $.ajax({
      url: '/admin/ajax/media/check/',
      data: {url: url}
    }).done(function(data){

      console.log('MediaURL: check data: %o', data);

      self.updated(data, uid);

    }).fail(function(jqxhr){
      // parse the response text into json because reasons
      var response = jQuery.parseJSON(jqxhr.responseText);
      self.setState({errorMessage: response.message});
      console.log('MediaBox: error returned from check: %o', response);
    });

  },

  updated: function(data, uid) {
    console.log('MediaBox: item checked, index = %o, data = %o', uid, data);

    var media = this.state.media;
    var f = filter_by_uid(media, function(m){return m.uid === uid});

    if (f === false) {
      return;
    }

    var index = media.indexOf(f);

    if (index === -1) {
      console.log('MediaBox: %o could not be found in array', f);
    }

    var updatedMedia = $.extend({}, f, data);

    media[index] = updatedMedia;

    this.setState({media: media});

  },

  // TODO
  removed: function(uid) {
    console.log('MediaBox: removing index %o', uid);

    var updatedMedia = this.state.media.filter(function(m) {
        return m.uid !== uid;
    });

    this.setState({media: updatedMedia});
  },

	render: function() {

		console.log('MediaBox: rendered');

    var self = this;
    var index = 0;

    var elements = this.state.media.map(function(media){
      var container = (
        <div key={media.uid}>
          <MediaURL uid={media.uid} url={media.url} onChecked={self.checked} />
          <MediaInformation uid={media.uid} media={media} onRemove={self.removed} index={index++}/>
        </div>
      );
      return container;
    });

    var errMsg = null;
    if (this.state.errorMessage) {
      console.log('MediaBox: rendering error message');
      errMsg = <MediaBoxError message={this.state.errorMessage} />
    }

		return (
			<div>
        {errMsg}
        {elements}
        <a href="#" onClick={this.addNewMedia}>New Media</a>
      </div>
		);
	}
});
