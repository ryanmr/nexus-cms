
/**
 * Get the global token if it is available.
 * @return {string} a string of the global csrf token
 */
function get_csrf_token() {
	var element = $('meta[name="_token"]');

	if (!element) {
		console.warn('global meta csrf token is not available');
		return '';
	}

	var token = element.attr('content').trim();
	return token;
}

/**
 * Generate a randomized identifier.
 *
 * Adapted from Foundation's random string generator.
 * Looking for a non-var declaration method though.
 *
 * @return string a randomized string that will not repeat locally
 */
var get_unique_identifier = (function(){
		var incrementer = 0;

		function generator() {
			var string = ['ui', (+new Date).toString(36), (incrementer++).toString(36)].join('-');

			return string;
		}

		return generator;
})();

function filter_by_uid(array, fn) {

	var filtered = array.filter(fn);

	if (filtered === 0) {
		console.log('filter_by_uid: %o did not exist in the array', uid);
		return false;
	}

	return filtered[0];
}

function _get_data_id(element, attribute) {
	var data_id = $(element).attr(attribute);
	if ( !data_id || data_id == -1 ) {
		console.info('%o may not contain a valid data-id', element);
		data_id = null;
	}
	return data_id;
}

/**
 * When a component is activated, insert a 'component activated' hidden input indicator nearby.
 * @param  element element where the element should be inserted after
 * @param  string key     the key that the hidden element should use
 */
function component_activation(element, key) {
	var input = $('<input />');
	var name = "components-activated[]";
	input.val(key);
	input.attr('name', name);
	input.attr('type', 'hidden');
	$(element).after(input);
}

/**
 * Formats a string in title case format.
 * @param  string str the string that should be formatted
 * @return string     a formatted string
 */
function toTitleCase(str) {
	// http://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
  return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
