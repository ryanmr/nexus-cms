var Vue = require('vue');

import Episode from './components/Episode.vue';


Vue.config.debug = true;

var app = new Vue({
  el: '#app',

  components: {Episode},

  created() {
    console.log('app: created');
  },

  ready() {
    console.log('app: ready');
  }
});
