

/**
 * Automatically add events to specifically marked anchor-links with the data-delete attribute.
 * Find all such links on a page, and add a click event that will dynamically create a form and submit
 * the proper inputs to the URL specified in the link's href.
 *
 */
function initialize_delete() {

	function get_elements() {
		var elements = $('a[data-delete]');

		if (!elements || elements.length <= 0) {
			[];
		}

		return elements;
	}

	function create(url, token, method) {
		var form = $('<form>', {method: 'POST', 'action': url});
		var token = $('<input>', {type: 'hidden', name: '_token', value: token});
		var method = $('<input>', {type: 'hidden', 'name': '_method', value: method});
		form = form.append(token, method);
		console.log(form);
		return form;
	}

	function inject(element) {
		$(document.body).append(element);
	}

	/*
		Require two clicks to make the form.
	*/
	function handle(event) {
		var target = $(event.target);
		if (target.hasClass('clicked')) {
			var link = $(this);
			var token = get_csrf_token();
			var href = link.attr('href');
			var form = create(href, token, 'delete');
			inject(form);
			form.submit();
		} else {
			target.addClass('alert').addClass('clicked').removeClass('warning');
			target.html('Confirm Delete');
		}
		event.preventDefault();
	}

	get_elements().on('click', handle);

}

/**
 * Initialize the React component for Related episodes.
 * @return {React Component}
 */
function initialize_related() {
	var target = $('.related-container').get(0);
	if (!target) return;

	console.log('episode related box detected');

	var data_id = _get_data_id(target, 'data-id');

	React.render(<RelatedBox episode_id={data_id} />, target);
	component_activation(target, 'related');
}

/**
 * Initialize the React component for People in episodes.
 * @return {React Component}
 */
function initialize_people() {
	var target = $('.people-container').get(0);
	if (!target) return;

	console.log('episode people box detected');

	var data_id = _get_data_id(target, 'data-id');

	React.render(<PeopleBox episode_id={data_id} />, target);
	component_activation(target, 'people');
}

/**
 * Initialize the React component for Media in episodes.
 * @return {React Component}
 */
function initialize_media() {
	var target = $('.media-container').get(0);
	if (!target) return;

	console.log('episode media box detected');

	var data_id = _get_data_id(target, 'data-id');

	React.render(<MediaBox episode_id={data_id} />, target);
	component_activation(target, 'media');
}

function initialize_dropzone() {
	var target = $('#artUpload').get(0);
	if (!target) return;

	console.log('artUpload for dropzone detected');

	Dropzone.options.artUpload = {
		maxFilesize: 4,
		acceptedFiles: '.jpg, .jpeg, .png'
	};
}

/**
 * Primarily ReactJS functionality.
 */
function initialize_components() {
	initialize_related();
	initialize_people();
	initialize_media();
	initialize_dropzone();
}

/**
 * Primarily jQuery/plain javascript functionality.
 */
function initialize_other() {
	initialize_delete();
}

/**
 * Run all of our code!
 */
$(document).ready(function(){

	initialize_components();
	initialize_other();

});
