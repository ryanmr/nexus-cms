var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 */

elixir(function(mix) {

    // copy image files if needed
    // mix.copy('resources/assets/css/{package}/images/', 'public/css/build/images/');

    // compile with sass the admin styles
    mix.sass('admin.scss', 'public/css/build/admin.css');

    // merge and compile the vendor files
    mix.styles([
      'vendor/foundation.min.css',
      'vendor/dropzone.css',
    ], 'public/css/build/vendor.css');

    // merge and compile with BabelJS the JavaScript
    mix.babel([
      'admin/global.js',
      'admin/admin.js'
    ], 'public/js/build/admin.js');

    mix.browserify('admin/components.js', 'public/js/build/components.js');

    // merge and compile the vendor JavaScript files: jQuery, Foundation, Vue
    mix.scripts([
      'vendor/jquery.js',
      'vendor/algolia-autocomplete.js',
      'vendor/modernizr.js',
      'vendor/fastclick.js',
      'vendor/foundation.min.js',
      'vendor/dropzone.js',
    ], 'public/js/build/vendor.js');

});
