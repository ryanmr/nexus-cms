<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model {

	protected $guarded = [];

	protected $table = 'people';

	public function episodes() {
		return $this->belongsToMany('App\Episode', 'people_episodes', 'person_id', 'episode_id');
	}

	public function series() {
		return $this->belongsToMany('App\Series', 'people_series', 'person_id', 'series_id');
	}

	public function scopeVisible($query) {
		return $query->where('hidden', '=', false);
	}

	public static function getPeopleList() {
		return self::orderBy('name', 'asc')->get();
	}

}
