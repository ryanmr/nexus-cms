<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumArtView extends Model
{

    protected $guarded = [];

    public function getViewablePath() {
      return $this->path . $this->filename;
    }

    public function getFilePath() {
      return public_path() . $this->path . $this->filename;
    }

    public function getDirectoryPath() {
      return public_path() . $this->path;
    }



}
