<?php

$router->pattern('series_slug', '[A-Za-z-]+');
$router->pattern('person_slug', '[A-Za-z-]+');

$router->pattern('episode_number', '\d+');

Route::group(['middleware' => ['web']], function () {

    Route::auth();

    Route::group(['prefix' => 'api', 'middleware' => 'auth'], function(){

    	Route::resource('people', 'Api\PeopleController');
    	Route::resource('series', 'Api\SeriesController');
    	Route::resource('episodes', 'Api\EpisodeController');
    	Route::resource('art', 'Api\AlbumArtController');

    	Route::get('media/check', ['as' => 'media-check', 'uses' => 'Api\MediaController@check']);
    });

    Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){

    	Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'Admin\DashboardController@index']);

    	Route::resource('series', 'Admin\SeriesController');

    	Route::resource('episodes', 'Admin\EpisodeController');

    	Route::resource('art', 'Admin\ArtController');

    	Route::resource('people', 'Admin\PeopleController');

    	Route::group(['prefix' => 'ajax'], function(){

    		Route::get('people/search/', 'Admin\AjaxController@peopleSearch');
    		Route::get('people/initial/', 'Admin\AjaxController@people');

    		Route::get('episode/related/search/', 'Admin\AjaxController@episodesSearch');
    		Route::get('episode/related/initial/', 'Admin\AjaxController@episodes');

    		Route::get('media/initial/', 'Admin\AjaxController@mediaInitial');
    		Route::get('media/check/', 'Admin\AjaxController@mediaCheck');

    		Route::post('art/upload/', 'Admin\ArtController@upload');
    	});

    });

    /**
     * Root routes.
     *
     * Routes that are attached to the /
     * - home
     * - latest
     * - admin shortcut
     */

    Route::get('admin', [
      'uses' => 'Frontend\RootController@admin'
    ]);

    Route::get('/', [
      'as' => 'home',
      'uses' => 'Frontend\RootController@home'
    ]);

    Route::get('/latest', [
      'as' => 'latest',
      'uses' => 'Frontend\RootController@latest'
    ]);

    /**
     * People routes.
     *
     * - people's full list
     * - a person's page
     */

    Route::get('/people/', [
      'as' => 'people',
      'uses' => 'Frontend\PeopleController@people'
    ]);

    Route::get('/person/{person_slug}/', [
      'as' => 'person',
      'uses' => 'Frontend\PeopleController@person'
    ]);

    /**
     * Episode routes.
     *
     * Most of these routes are functionally redirects.
     *
     * - /episode/{s}{n}; legacy format
     * - /e/{id}; shortcut format
     * - /{s}{n}; legacy shortcut format
     * - /{s}/{n}; new format (that everyone loves)
     */

    Route::get('/episode/{series_slug}{episode_number}', [
      'uses' => 'Frontend\EpisodesController@episodeLegacyRedirect'
    ]);

    Route::get('/e/{id}', [
      'uses' => 'Frontend\EpisodesController@episodeByIdRedirect'
    ]);

    Route::get('/{series_slug}{episode_number}/', [
      'uses' => 'Frontend\EpisodesController@episodeLegacyRedirect'
    ]);

    Route::get('/{series_slug}/{episode_number}/', [
      'as' => 'episode',
      'uses' => 'Frontend\EpisodesController@episode'
    ]);

    /**
     * Series routes.
     *
     * - series list
     * - single series (redirect)
     * - single series
     */

    Route::get('/series', [
      'as' => 'series-list',
      'uses' => 'Frontend\SeriesController@seriesList'
    ]);

    Route::get('/series/{series_slug}', [
      'as' => 'series',
      'uses' => 'Frontend\SeriesController@seriesSubslug'
    ]);

    Route::get('/{series_slug}/', [
      'as' => 'series',
      'uses' => 'Frontend\SeriesController@series'
    ]);

    Route::get('/master/feed', [
      'as' => 'master.feed',
      'uses' => 'Frontend\FeedsController@feedMaster'
    ]);

    /**
     * Route for series/feed.
     *
     * This route fetches the published episodes without fringes
     * of the specified episode.
     */
    Route::get('/{series_slug}/feed', [
      'as' => 'series.feed',
      'uses' => 'Frontend\FeedsController@feedGeneral'
    ]);

    /**
     * Route for series/feed/fringe.
     *
     * This route gets the latest episodes and their fringes if they are available,
     * and merges them into a single collection for the feed.
     *
     * TODO: fix this awful query mess
     * TODO: really, why haven't you fixed this yet?
     */
    Route::get('/{series_slug}/feed/fringe', [
      'as' => 'series.feed.fringe',
      'uses' => 'Frontend\FeedsController@feedFringe'
    ]);

});
