<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Components\Media\MP3Checker;

class MediaController extends Controller
{

  public function check(Request $request, MP3Checker $checker) {
		$url = trim($request->input('url', null));

    return \Response::json(
      [
        'data' => [
          'media' => $checker->check($url)
        ]
      ]
    );
	}

}
