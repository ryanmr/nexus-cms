<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Episode as Episode;

class EpisodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $query = $request->input('q');

      if ($query) {
        $episodes = \App\Episode::where('title', 'LIKE', "%$query%")->take(10)->orderBy('id', 'desc')->get();
      } else {
        $episodes = \App\Episode::orderBy('id', 'desc')->get();
      }

      return \Response::json(
        [
          'data' => [
            'episodes' => $episodes
          ]
        ]
      );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $input = $request->input('episode');
      $relationships = ['art', 'series', 'people', 'related', 'media'];
      $core = array_except($input, $relationships);


      $episode = Episode::create($core);

      $episode->save();

      $episode = $this->saveRelated($episode, $request);

      // fetch a clean object and return it

      $e = Episode::with($relationships)->find($episode->id);

      return \Response::json(
        [
          'data' => [
            'episode' => $e->toArray()
          ],
          'method' => 'create'
        ]
      );

    }


    private function saveRelated(Episode $episode, Request $request) {
      $episode->people()->sync($request->input('episode.people', []), !$episode->wasRecentlyCreated);

      $episode->related()->sync($request->input('episode.related', []));

      $this->saveMedia($episode, $request->input('episode.media', []));

      return $episode;
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $episode = \App\Episode::with(['series', 'people', 'related', 'media', 'art'])->findOrFail($id);

      return \Response::json(
        [
          'data' => [
            'episode' => $episode
          ],
          'method' => 'show'
        ]
      );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $input = $request->input('episode');
      $relationships = ['art', 'series', 'people', 'related', 'media'];
      $core = array_except($input, $relationships);

      if (empty($core['album_art_id'])) {
        $core['album_art_id'] = null;
      }

      $episode = Episode::findOrFail($id);

      $episode->update($core);

      $this->saveRelated($episode, $request);

      // fetch a clean object and return it

      $e = Episode::with($relationships)->find($episode->id);

      return \Response::json(
        [
          'data' => [
            'episode' => $e->toArray()
          ],
          'method' => 'update'
        ]
      );
    }

    private function saveMedia(Episode $episode, $media) {
  		// media id accumulator, used for syncing existing and deleted media
  		$mediaIds = [];

  		// iterate througth the media just sent up on this request
  		foreach ($media as $data) {
  			$id = array_get($data, 'id');

  			// fields that should not be here are ignored through Model::guarded

  			if ($id) {
  				$episodeMedia = \App\EpisodeMedia::find($id);
  				// the suggested id was invalid, so skip this iteration
  				if ($episodeMedia == null) {
  					continue;
  				}
  				$episodeMedia->update($data);
  				$mediaIds[] = $id;
  			} else {
  				// the item does not have an id field, so it must be a new entry
  				$episodeMedia = new \App\EpisodeMedia($data);
  				$mediaIds[] = $episodeMedia->id;
  			}

  			// save the media to this episode
  			$episode->media()->save($episodeMedia);

  		}

  		// using the accumulated ids, remove the ids that were not involved in this transaction
  		$episode->media()->whereNotIn('id', $mediaIds)->delete();
  	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
