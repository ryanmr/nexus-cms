<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Components\Media\MP3Checker;

class AjaxController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index() {

		$array = [1,2,3];

    return $array;
	}

	public function mediaInitial(Request $request) {
		$id = $request->input('id', null);

		if ( $id == null || $id == false ) {
			return [];
		}

		$episode = \App\Episode::with(['media'])->findOrFail($id);

		return $episode->media;
	}

	public function mediaCheck(Request $request, MP3Checker $checker) {
		$url = trim($request->input('url', null));
		return $checker->check($url);
	}

  public function people(Request $request) {
		$id = $request->input('id', null);

		if ( $id == null || $id == false ) {
			return [];
		}

    $episode = \App\Episode::with('people')->findOrFail($id);

    return $episode->people;
  }

  public function peopleSearch(Request $request) {
		$term = $request->input('term', null);

		if ($term == null || trim($term) == '') {
			return [];
		}

    $people = \App\Person::where('name', 'LIKE', "%{$term}%")->take(10)->get();

		$results = [];

		foreach ($people as $person) {
			$result = ['label' => $person->name, 'value' => $person->id];
			$results[] = $result;
		}

    return $results;
  }

  public function episodes(Request $request) {
		$id = $request->input('id', null);

		if ( $id == null ) {
			return [];
		}

    $episode = \App\Episode::with('related')->findOrFail($id);

    return $episode->related;
  }

  public function episodesSearch(Request $request) {
		// parent in this case indicates non-fringe
		$type = $request->input('type', 'parent');

		$term = trim($request->input('term', ''));

		if ( $term == '' ) {
			return [];
		}

		$query = \App\Episode::with('series')->where('name', 'LIKE', "%{$term}%")->orderBy('id', 'desc')->take(10);

		// if ($type == 'fringe') {
		// 	$query = $query->where('slug', '=', 'fringe');
		// } else {
		// 	$query = $query->where('slug', '!=', 'fringe');
		// }

    $episodes = $query->get();

		$results = [];

		foreach ($episodes as $episode) {
			$result = ['label' => $episode->formal_name, 'value' => $episode->id];
			$results[] = $result;
		}

		return $results;
  }

}
