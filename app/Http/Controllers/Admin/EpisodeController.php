<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\FormComponentVerifier as ComponentVerifier;

class EpisodeController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	private function getRequestInput(Request $request) {
		$except = [
			'components-activated',
			'art'
		];
		return $request->except($except);
	}

	public function index() {
		$episodes = \App\Episode::orderBy('created_at', 'DESC')->paginate(20);
		$series = \App\Series::orderBy('name', 'DESC')->get();

		return view('admin.episodes.index', [
			'episodes' => $episodes,
			'series' => $series
		]);
	}

	public function show($id) {
		return redirect(route('admin.episodes.index'));
	}

	public function create() {
		// $series = \App\Series::usable()->lists('name', 'id');
		//
		// $array = ['series' => $series];
		// return view('admin.episodes.create', $array);
		return view('admin.episodes.create');
	}

	public function store(Request $request, ComponentVerifier $verifier) {

		$input = $this->getRequestInput($request);

		$this->_validate($request);

		$episode = \App\Episode::create($input);

		$episode->save();

		$this->saveComponents($episode, $request, $verifier);

		$this->saveArt($episode, $request);

		return redirect(route('admin.episodes.edit', $episode->id))
		->with(['flash.message' => 'Episode created!',
				'flash.level'	=> 'success']);
	}

	public function edit($id) {

		$episode = \App\Episode::with(['series', 'media'])->findOrFail($id);

		$series = \App\Series::usable()->lists('name', 'id');

		$array = ['series' => $series, 'episode' => $episode];
		return view('admin.episodes.edit', $array);

	}

	public function update(Request $request, ComponentVerifier $verifier, $id) {

		$input = $this->getRequestInput($request);

		$this->_validate($request);

		$episode = \App\Episode::findOrFail($id);

		$episode->update($input);

		$this->saveComponents($episode, $request, $verifier);

		$this->saveArt($episode, $request);

		return redirect(route('admin.episodes.edit', $episode->id))
		->with(['flash.message' => 'Episode updated!',
				'flash.level'	=> 'success']);
	}

	private function saveComponents(\App\Episode $episode, Request $request, ComponentVerifier $verifier) {
		if ($verifier->verify($request, 'people')) {
			$people = $request->input('people', []);
			$episode->people()->sync($people);
		}

		if ($verifier->verify($request, 'related')) {
			$related = $request->input('related', []);
			$episode->related()->sync($related);
		}

		if ($verifier->verify($request, 'media')) {
			$media = $request->input('media', []);
			// this method is on the controller and not elsewhere
			// because it processes the form structure more specifically than would be required
			// if the media was being saved manually
			$this->saveMedia($episode, $media);
		}
	}

	private function saveArt($episode, $request) {
		$art = $request->input('art');
		if (!empty($art['album_art_id'])) {
			$episode->art()->sync([$art['album_art_id']]);
		}
	}

	private function saveMedia($episode, $media) {

		// media id accumulator, used for syncing existing and deleted media
		$mediaIds = [];

		// iterate througth the media just sent up on this request
		foreach ($media as $data) {
			$id = array_get($data, 'id');

			// fields that should not be here are ignored through Model::guarded

			if ($id) {
				$episodeMedia = \App\EpisodeMedia::find($id);
				// the suggested id was invalid, so skip this iteration
				if ($episodeMedia == null) {
					continue;
				}
				$episodeMedia->update($data);
				$mediaIds[] = $id;
			} else {
				// the item does not have an id field, so it must be a new entry
				$episodeMedia = new \App\EpisodeMedia($data);
				$mediaIds[] = $episodeMedia->id;
			}

			// save the media to this episode
			$episode->media()->save($episodeMedia);

		}

		// using the accumulated ids, remove the ids that were not involved in this transaction
		$episode->media()->whereNotIn('id', $mediaIds)->delete();
	}

	private function _validate(Request $request) {
		$this->validate($request, [
			'name' => 'required|min:2|max:140',
			'series_id' => 'required|integer',
			'number' => 'required|integer',
			'description' => 'max:300'
		]);
	}

	public function destroy(Request $request, $id) {

		$episode = \App\Episode::findOrFail($id);

		$episode->delete();

		return redirect(route('admin.episodes.index', $episode->id))
		->with(['flash.message' => 'Episode deleted!',
				'flash.level'	=> 'success']);

	}

}
