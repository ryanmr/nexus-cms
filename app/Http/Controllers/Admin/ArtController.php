<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $arts = \App\AlbumArt::with('views')->whereHas('views', function($query){
          $query->where('name', '=', 'small');
        })->orderBy('created_at', 'desc')->get();

        return view('admin.art.index', ['arts' => $arts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('admin.art.create');
    }

    /**
     * Handle a new uploaded art file!
     *
     * 1. Validate that the file is a proper image
     * 2. The art is untitled coming up, so give it a fake name for now.
     * 3. Save the uploaded art using Job/SaveUploadedArt.
     * 4. Save the resized copies based on the original using Job/ResizeArt.
     * 5. Return the art data.
     *
     * @return Response
     */
    public function upload(Request $request) {

      $this->validate($request, [
          'file' => 'required|mimes:jpg,jpeg,png,gif'
      ]);

      $file = $request->file('file');
      $name = strtolower($file->getClientOriginalName());

      $art = new \App\AlbumArt(['name' => "New: {$name}"]);
      $art->save();

      $this->dispatch(new \App\Jobs\SaveUploadedArt($art, $file));

      $this->dispatch(new \App\Jobs\ResizeArt($art));

      return $art;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $art = \App\AlbumArt::with(['views', 'series'])->findOrFail($id);

        return view('admin.art.edit', ['art' => $art]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
          'name' => 'required|max:50'
      ]);

      $input = $request->except('series');

      $art = \App\AlbumArt::with(['views', 'series'])->findOrFail($id);

      $art->update($input);

      return redirect(route('admin.art.edit', $art->id))
  		->with(['flash.message' => 'Art updated!',
  				'flash.level'	=> 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
