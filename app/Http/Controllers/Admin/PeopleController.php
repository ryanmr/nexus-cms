<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
      $people = \App\Person::orderBy('created_at', 'desc')->get();

      return view('admin.people.index', ['people' => $people]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('admin.people.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
      $input = $request->all();

      $person = \App\Person::create($input);

      $person->save();

      return redirect(route('admin.people.edit', $person->id))
          ->with(['flash.message' => 'Person updated!',
              'flash.level'	=> 'success']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $person = \App\Person::findOrFail($id);

      return view('admin.people.edit', ['person' => $person]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();

        $person = \App\Person::findOrFail($id);

      	$person->update($input);

        return redirect(route('admin.people.edit', $person->id))
    				->with(['flash.message' => 'Person updated!',
    						'flash.level'	=> 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
      $person = \App\Person::findOrFail($id);

      $person->delete();

      return redirect(route('admin.people.index'))
        ->with(['flash.message' => 'Person deleted!',
            'flash.level'	=> 'success']);
    }
}
