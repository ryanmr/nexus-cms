<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Request;

class SeriesController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index() {
		$series = \App\Series::orderBy('name')->get();

		$array = ['series' => $series];
		return view('admin.series.index', $array);
	}

	public function show($id) {
		return redirect(route('admin.series.index'));
	}

	public function create() {
		return view('admin.series.create');
	}

	public function store(Request $request) {

		$fields = $request->except(['meta', 'art']);

		$series = \App\Series::create($fields);

		$meta = \App\SeriesMeta::create($request->input('meta'));

		$series->meta()->save($meta);

		$this->saveArt($series, $request);

		return redirect(route('admin.series.edit', $series->id))
				->with(['flash.message' => 'Series created!',
						'flash.level'	=> 'success']);
	}

	public function edit($id) {
		$series = \App\Series::with(['meta', 'art', 'hosts'])->findOrFail($id);

		$array = ['series' => $series];
		return view('admin.series.edit', $array);
	}

	public function update($id, Request $request) {

		$series = \App\Series::with(['meta', 'art'])->findOrFail($id);

		$fields = $request->except(['meta', 'art']);

		$series->update($fields);

		$series->meta()->update($request->input('meta'));

		$this->saveArt($series, $request);

		$this->saveHosts($series, $request);

		return redirect(route('admin.series.edit', $series->id))
			->with(['flash.message' => 'Series updated!',
					'flash.level'	=> 'success']);
	}

	private function saveHosts($series, $request) {
		$hosts = $request->input('people_default', []);
		$series->hosts()->sync($hosts);
	}

	private function saveArt($series, $request) {
		$art = $request->input('art_list', []);

		// force items in the array to be numeric - such that they are ids
		$filtered = array_filter($art, function($item){
			return is_numeric($item);
		});

		$series->art()->sync($filtered);
	}

	public function destroy($id) {
		$series = \App\Series::with('meta')->findOrFail($id);

		$series->delete();

		return redirect(route('admin.series.index', $series->id))
			->with(['flash.message' => 'Series deleted!',
					'flash.level'	=> 'success']);
	}

}
