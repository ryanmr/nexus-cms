<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Request;

class EpisodeMediaController extends Controller {

	public function show($id) {
		$episode = \App\Episode::with(['media'])->findOrFail($id);

		return $episode;
	}

}
