<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EpisodesController extends Controller
{

  public function episode($series_slug, $episode_number) {
    $episode = \App\Episode::with(['series', 'people', 'related', 'art', 'media'])
      ->accessible()
      ->withSlug($series_slug)
      ->where('number', '=', $episode_number)
      ->firstOrFail();

    // return $episode;
    return view('frontend.episode.landing', ['episode' => $episode]);
  }

  public function episodeLegacyRedirect($series_slug, $episode_number) {
    return redirect("$series_slug/$episode_number");
  }

  public function episodeByIdRedirect($id) {
    $episode = \App\Episode::with(['series'])
      ->where('id', '=', $id)
      ->firstOrFail();
    $number = $episode->number;
    $series_slug = $episode->series->slug;
    return redirect("$series_slug/$number");
  }

}
