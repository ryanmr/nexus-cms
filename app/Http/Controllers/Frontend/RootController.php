<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RootController extends Controller
{

  public function latest()
  {
    $episodes = \App\Episode::visible()
      ->recent()
      ->paginate(15);

    return $episodes;
  }

  public function home()
  {
    $episodes = \App\Episode::recent()->take(10)->get();

    return view('frontend.welcome', ['episodes' => $episodes]);
  }

  public function admin()
  {
    return redirect('/admin/dashboard');
  }

}
