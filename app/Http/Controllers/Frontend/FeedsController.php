<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FeedsController extends Controller
{

  public function feedMaster() {
    $episodes = \App\Episode::visible()->recent()->get();

    return view('frontend.feeds.master', ['episodes' => $episodes]);
  }

  public function feedGeneral($series_slug) {
          $series = \App\Series::with(['episodes' => function($query){
              $query->visible()->recent();
            }])
            ->visible()
            ->whereSlug($series_slug)
            ->firstOrFail();

        		return view('frontend.feeds.primary', ['series' => $series, 'episodes' => $series->episodes]);
  }

  public function feedFringe($series_slug) {

          // get the series information
          $series = \App\Series::visible()
        		->withSlug($series_slug)
        		->firstOrFail();

          // get all of the recent episodes
          // related are loaded because they are queried next
        	$episodes = \App\Episode::with(['related'])
        		->visible()
        		->recent()
        		->withSlug($series_slug)
        		->get();

          // build a list of ids that need to be included
          // the episode itself and the fringe of that episode
        	$included_ids = array();
        	foreach ($episodes as $episode) {
        		$included_ids[] = $episode->id;
        		if ( $episode->related->count() > 0 ) {
        			foreach ($episode->related as $related) {
        				if ( $related->pivot->type == 'fringe' ) {
        					$included_ids[] = $related->id;
        				}
        			}
        		}
        	}

          /**
           * Ideally, the included_ids or the following query would be cached,
           * and only cleared if something was saved.
           */

          /*
           * TODO:
           * Convert this multi-query version to the better unified SQL single query version.
           */

          $episodes = \App\Episode::with(['related', 'media', 'series'])
          ->visible()
          ->recent()
          ->whereIn('id', $included_ids)
          ->get();

        	return view('frontend.feeds.primary', ['series' => $series, 'episodes' => $episodes]);
  }


}
