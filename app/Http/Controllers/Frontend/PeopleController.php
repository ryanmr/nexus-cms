<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PeopleController extends Controller
{

  public function person() {
    $person = \App\Person::visible()
    ->where('slug', '=', $person_slug)
    ->firstOrFail();

    return $person;
  }

  public function people()
  {
    $people = \App\Person::visible()
      ->paginate(15);

    return $people;
  }

}
