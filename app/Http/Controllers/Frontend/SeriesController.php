<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SeriesController extends Controller
{

  public function series($series_slug) {
    $series = \App\Series::with(['episodes' => function($query){
        $query->visible()->recent();
      }])
      ->visible()
      ->where('slug', '=', $series_slug)
      ->firstOrFail();

    // return $series;
    return view('frontend.series.landing', ['series' => $series]);
  }

  public function seriesSubslug($series_slug) {
    return redirect("/{$series_slug}");
  }

  public function seriesList() {
    $series = \App\Series::visible()->get();

    return view('frontend.series.list', ['series' => $series]);
  }

}
