<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EpisodeMedia extends Model {

	public $timestamps = false;

	protected $table = 'episodes_medias';

	protected $guarded = ['human_length', 'human_size', 'uid'];

	protected $appends = ['human_length', 'human_size'];

	public function episode() {
		return $this->belongsTo('App\Episode', 'episode_id');
	}

	public function getMimeAttribute($value) {
		if ($this->type == 'mp3' && ($value == '') || ($value == null)) {
			$value = 'audio/mpeg';
		}

		return $value;
	}

	public function getHumanLengthAttribute() {
		$init = $this->length;
		$hours = floor($init / 3600);
		$minutes = floor(($init / 60) % 60);
		$seconds = $init % 60;
		return "$hours:$minutes:$seconds";
	}

	public function getHumanSizeAttribute() {
		$bytes = $this->size;

		$size   = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
    $factor = floor((strlen($bytes) - 1) / 3);

    return sprintf("%.2f ", $bytes / pow(1024, $factor)) . @$size[$factor];
	}

}
