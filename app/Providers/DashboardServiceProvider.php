<?php

namespace App\Providers;

use Event;
use Illuminate\Support\ServiceProvider;
use App\Components\Playboard;
use App\Components\Recents;

class DashboardServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Playboard $playboard, Recents $recents) {

      /**
       * Clear the caches of the Playboard and Recents.
       */
      $clear = function ($episode) use ($playboard, $recents) {

        $playboard->expire();
        $recents->expire();

        return true;
      };

      // Apply the events
      \App\Episode::saved($clear);
      \App\Episode::deleted($clear);
      \App\Series::saved($clear);
      \App\Series::deleted($clear);
    }

    /**
     * Registering various services for the Dashboard.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('playboard', function($app) {
          return new \App\Components\Playboard();
        });

        $this->app->singleton('recents', function($app){
          return new \App\Components\Recents();
        });
    }
}
