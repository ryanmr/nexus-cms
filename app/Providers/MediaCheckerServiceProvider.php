<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MediaCheckerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {

    }

    /**
     * Registering various services for the Dashboard.
     *
     * @return void
     */
    public function register()
    {
      $this->app->bind('\App\Components\Media\MP3Checker', function($app){
        return new \App\Components\Media\MP3Checker(new \GuzzleHttp\Client(), new \getID3());
      });
    }
}
