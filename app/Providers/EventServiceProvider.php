<?php namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use App\Episode;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	 protected $listen = [
			'App\Events\EpisodeWasCreated' => [
				'App\Listeners\ApplyEpisodeDefaults',
			],
			'App\Events\EpisodeWasSaved' => [
				'App\Listeners\PublishEpisode',
			],
		];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		Episode::created(function(Episode $episode){
			event(new \App\Events\EpisodeWasCreated($episode));
			return true;
		});

		Episode::saving(function(Episode $episode){
			event(new \App\Events\EpisodeWasSaved($episode));
			return true;
		});
	}

}
