<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SeriesMeta extends Model {

	public $timestamps = false;

	protected $table = 'series_metas';
	
	protected $guarded = [];

	public function series() {
		return $this->belongsTo('App\Series', 'series_id');
	}
}
