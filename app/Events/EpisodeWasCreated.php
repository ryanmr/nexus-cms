<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Episode;

class EpisodeWasCreated extends Event
{
    use SerializesModels;

    private $episode;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Episode $episode)
    {
      $this->episode = $episode;
    }

    public function getEpisode() {
      return $this->episode;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
