<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
// use Log;

use App\Episode;

use App\Events\EpisodeWasCreated;

class ApplyEpisodeDefaults
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EpisodeWasCreated  $event
     * @return void
     */
    public function handle(EpisodeWasCreated $event)
    {
      $episode = $event->getEpisode();

      $this->saveArt($episode);
      $this->saveHosts($episode);
    }

    private function saveArt(Episode $episode) {
      $art = $episode->art;

      // detect if the episode has any art attach
      // if not, attach series art if available
      if ($episode->art == null) {
        $series_art = $episode->series->art;
        if ($series_art->count() > 0) {
          $episode->album_art_id = $series_art->first()->id;
          $episode->save();
        }
      }

    }

    private function saveHosts(Episode $episode) {
      $people = $episode->people;

      if (count($people) == 0) {
        $series_hosts = $episode->series->hosts;
        if ($series_hosts->count() > 0) {

          $ids = $series_hosts->map(function($i, $k){
            return $i->id;
          })->all();

          $data = [];
          foreach ($ids as $key => $id) {
            $data[$id] = ['role' => 'host'];
          }

          $saved = $episode->people()->sync($data);
        }
      }

    }

}
