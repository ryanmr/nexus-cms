<?php

namespace App\Listeners;

use App\Events\EpisodeWasSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Episode;

class PublishEpisode
{

    const STATE = 'state';
    const PUBLISHED = 'published';
    const DRAFT = 'draft';

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(){}

    /**
     * Handle the event.
     *
     * @param  EpisodeWasSaved  $event
     * @return void
     */
    public function handle(EpisodeWasSaved $event)
    {
        $episode = $event->getEpisode();

        $didStateChange = $this->stateChanged(
          $episode->getOriginal(self::STATE, self::DRAFT),
          $episode->state);

        if ($didStateChange) {
            $this->publish($episode);
        }
    }

    private function stateChanged($previous, $next)
    {
      return $previous != self::PUBLISHED && $next == self::PUBLISHED;
    }

    private function publish(Episode $episode)
    {
      $episode->published_at = $episode->freshTimestamp();
      \Log::info('episode published',
        ['episode_id' => $episode->id,
         'published_at' => $episode->published_at]);
    }
}
