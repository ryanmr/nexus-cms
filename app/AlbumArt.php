<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AlbumArt extends Model
{

    protected $guarded = [];

  	protected $table = 'album_art';

    public function views() {
      return $this->hasMany('App\AlbumArtView');
    }

    public function series() {
      return $this->belongsToMany('\App\Series', 'album_art_series', 'album_art_id', 'series_id');
    }

    public function getSeriesListAttribute() {
      return $this->series->lists('id')->toArray();
    }

    public static function getArtList() {
      return self::with(['views'])->orderBy('created_at', 'asc')->get();
    }

    public function getView($key = 'small') {
      $view = $this->views->filter(function($v) use ($key) {
        return $key == $v->name;
      })->first();
      return $view;
    }

    public static function clearAll() {
      $views = \App\AlbumArtView::all();
      foreach ($views as $view) {
        $view->delete();
      }
      $arts = \App\AlbumArt::all();
      foreach ($arts as $art) {
        $art->delete();
      }
    }

}
