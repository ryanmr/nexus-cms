<?php

namespace App\Components;

use Carbon\Carbon;
use Cache;

class Recents {

  public function __construct() {

  }

  /**
   * Get the 4 most recent episodes, cached.
   * @return array a list of recent episodes
   */
  public function getRecentlyPublishedEpisodes() {
    $expiration = Carbon::now()->addDays(1);
    $episodes = Cache::remember('recents-episodes', $expiration, function(){
      $episodes = \App\Episode::with('series')->recent()->visible()->take(4)->get();
      return $episodes;
    });
    return $episodes;
  }

  public function expire() {
    Cache::forget('recents-episodes');
  }

}
