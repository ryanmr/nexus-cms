<?php

namespace App\Components;

use Carbon\Carbon;
use Cache;

class Playboard {

  public function __construct() {
    // empty
  }

  public function getSeries() {
    $expiration = Carbon::now()->addDays(1);
    $series = Cache::remember('playboard-series-count', $expiration, function(){
      $series = \App\Series::with('episodes')->visible()->orderBy('slug')->has('episodes')->get();
      return $series;
    });

    return $series;
  }

  public function getTotalEpisodesCount() {
    $count = \App\Episode::visible()->count();
    return $count;
  }

  public function getQuarterlyEpisodesCount() {
    $count = $this->getRangeCount(90);
    return $count;
  }

  public function getMonthlyEpisodesCount() {
    $count = $this->getRangeCount(30);
    return $count;
  }

  public function getWeeklyEpisodesCount() {
    $count = $this->getRangeCount(7);
    return $count;
  }

  private function getRangeCount($range) {
    $then = Carbon::now()->subDays($range);
    $now = Carbon::now();

    $count = \App\Episode::visible()->where(function($query) use ($then, $now) {
      $query->where('created_at', '>', $then)
            ->where('created_at', '<', $now);
    })->count();

    return $count;
  }

  public function getPeriodCount($key) {
    $expiration = Carbon::now()->addDays(1);
    $value = Cache::remember('playboard-period-counts', $expiration, function() {
    $array = [
        'quaterly' => $this->getQuarterlyEpisodesCount(),
        'monthly' => $this->getMonthlyEpisodesCount(),
        'weekly' => $this->getWeeklyEpisodesCount(),
        'total' => $this->getTotalEpisodesCount()
    ];
      return $array;
    });

    // TODO: guard against invalid keys
    return $value[$key];

  }

  public function expire() {
    Cache::forget('playboard-series-count');
    Cache::forget('playboard-period-counts');
  }

}
