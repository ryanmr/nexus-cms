<?php namespace App\Components\Media;

use App\Components\Media\MediaChecker;
use GuzzleHttp\Exception\RequestException;

class MP3Checker implements MediaChecker {

  protected $guzzle;

  protected $id3;

  // high threshold of bytes to download
  const DOWNLOAD_BYTES_MAX = 1048576;

  // how many bytes to read per read
  const DOWNLOAD_BYTES_READ = 1024;

  public function __construct($guzzle, $id3) {
    $this->guzzle = $guzzle;
    $this->id3 = $id3;
  }

  /**
   * Check the given URL for attributes as an array as describe in the interface:
   * - length/duration in seconds
   * - size in bytes
   * - mime/type
   * - overall type
   * @param  string $url the url of the media
   * @return
   */
  public function check($url) {
    // check for bad urls
    // TODO: ignore local files?
    if ($url == null || trim($url) == '') {
      return $this->error('URL was invalid.');
    }

    try {
      // initialize the response and stream
      $response = $this->guzzle->get($url, ['stream' => true]);

      // if the server does not support Content-Length, error
      if (!$response->hasHeader('Content-Length')) {
        return $this->error('Content length was not supplied from the remote host.');
      }

      $size = $response->getHeader('Content-Length')[0];

      // read the response body in pieces
      $body = $response->getBody();
      $bytes_read = 0;
      $bytes_to_read = self::DOWNLOAD_BYTES_MAX;
      $contents = '';
      // TODO: move data directly to the file
      while (!$body->eof() && $bytes_read < $bytes_to_read) {
          $r = self::DOWNLOAD_BYTES_READ;
          $contents .= $body->read($r);
          $bytes_read += $r;
      }
    } catch (RequestException $e) {
      return $this->error('There was an error while processing the remote file.');
    }

    // save the content to a file for ID3 analysis
    $tmpFilename = 'tmp-'.time().'.mp3';

    \Storage::disk('local')->put($tmpFilename, $contents);

    // delete contents
    unset($contents);

    // analyze the file with getID3
    $results = $this->id3->analyze(storage_path() . "/app/{$tmpFilename}", $size);

    // delete the file
    \Storage::disk('local')->delete($tmpFilename);

    // if the file is not encoded with playtime_seconds, error
    if (!array_key_exists('playtime_seconds', $results)) {
      return $this->error('The remote file did not have a "play time" attribute and may not be encoded properly.');
    }

    $length = round($results['playtime_seconds']);

    // return the structured data
    $data = [
      'size' => $size,
      'length' => $length,
      'type' => 'mp3',
      'mime' => 'audio/mpeg',
      'url' => $url
    ];

    return $data;
  }

  /**
   * Return an error message.
   * @param  string $message a message describing the error
   * @return array
   */
  public function error($message) {
    return [
        'type' => 'mp3',
        'error' => true,
        'message' => $message
    ];
  }

}
