<?php namespace App\Components\Media;

interface MediaChecker {

  /**
   * Check the given URL for valid media, and somehow extract the following data:
   * - the duration or length of the media in seconds
   * - the size in bytes
   * - the mime/type
   * - the generalized type
   * - and although given as an argument, the non-redirected, true URL
   * @param  string $url the url of the media
   * @return array      an array of the aforementioned contents
   */
  public function check($url);

}
