<?php

namespace App\Jobs;

use Illuminate\Support\Str;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Symfony\Component\HttpFoundation\File\File;

use \App\AlbumArt;

class ResizeArt extends Job implements SelfHandling
{

    private $art;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(AlbumArt $art)
    {
        $this->art = $art;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      // get the original album art
      $original = $this->art->getView('original');

      // find the extension of the file
      $file = new File($original->getFilePath());

      $extension = $file->getExtension();
      $name = Str::slug(basename($file->getFilename(), ".{$extension}"));


      // read the image into Intervention Image
      $image = \Image::make($original->getFilePath());

      // after each iteration, return back to the original image
      $image->backup();

      // get the list of sizes and widths
      $sizes = config('nexus.art.sizes');
      foreach ($sizes as $size => $width) {

        // resize the image, based on width
        // avoid accidental upscaling
        $image->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        // create the album art view
        $view = new \App\AlbumArtView();
        $view->name = $size;
        $view->filename = "{$name}-{$size}.{$extension}";
        $view->path = config('nexus.art.location.web');
        $view->width = $image->width();
        $view->height = $image->height();

        // save the album art view
        $this->art->views()->save($view);

        // save the changed image
        $image->save($view->getFilePath());

        // reset the image back to the original
        $image->reset();
      }

    }
}
