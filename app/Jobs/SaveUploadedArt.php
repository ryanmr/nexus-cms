<?php

namespace App\Jobs;

use Illuminate\Support\Str;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SaveUploadedArt extends Job implements SelfHandling
{

    private $art;
    private $file;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\AlbumArt $art, UploadedFile $file)
    {
        $this->art = $art;
        $this->file = $file;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      // get image extension
      $extension = $this->file->guessExtension();
      $clientExtension = $this->file->getClientOriginalExtension();

      $name = Str::slug(basename($this->file->getClientOriginalName(), ".{$clientExtension}"));

      // create the new view
      $view = new \App\AlbumArtView();
      $view->name = 'original';
      $view->filename = "{$name}-{$this->art->id}.{$extension}";
      $view->path = config('nexus.art.location.web');

      // move the file to its new location
      $this->file->move($view->getDirectoryPath(), $view->filename);

      // get the image data to continue
      $image = \Image::make($view->getFilepath());

      // set the width and height
      $view->width = $image->width();
      $view->height = $image->height();

      // save the view onto the art
      $this->art->views()->save($view);
    }
}
