<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Events\EpisodeWasCreated;

class Episode extends Model {

	 use SoftDeletes;

	/**
	 * An array containing all of the relationships we want to ignore.
	 * @var array
	 */
	public static $ignore = ['art', 'people', 'related', 'media', 'series', 'formal_name'];

	/**
	 * An array of model properties that should not be assumed to exist on the model itself.
	 * For instance, people is not a property of the model, but instead of another table via join.
	 *
	 * @var array
	 */
	protected $guarded = ['people', 'related', 'media', 'art', 'components-activated'];

	protected $hidden = [];

	protected $dates = ['published_at', 'deleted_at'];

	/**
	 * Add the specified custom attributes to the toArray/toJson versions of this model.
	 * @var array
	 */
	protected $appends = ['formal_title'];

	/* Relationships */
	public function related() {
		return $this->belongsToMany('App\Episode', 'episodes_relations', 'episode_id', 'episode_related_id')->withPivot('type');
	}

	public function series() {
		return $this->belongsTo('App\Series', 'series_id');
	}

	public function media() {
		return $this->hasMany('App\EpisodeMedia', 'episode_id');
	}

	public function people() {
		return $this->belongsToMany('App\Person', 'people_episodes', 'episode_id', 'person_id')->withPivot('role');
	}

	public function art() {
		return $this->belongsTo('App\AlbumArt', 'album_art_id');
	}

	public function getDefaultArt() {
		return $this->series->art;
	}

	/* Scopes */
	public function scopeVisible($query) {
		return $query
				->where('hidden', '=', false)
				->where('unlisted', '=', false)
				->where('state', '=', 'published');
	}

	public function scopeAccessible($query) {
		return $query
				->where('hidden', '=', false)
				->where(function($query){
					$query
						->where('state', '=', 'preview')
						->orWhere('state', '=', 'published');
				});
	}

	public function scopeRecent($query) {
		return $query->orderBy('published_at', 'DESC')->orderBy('created_at', 'DESC');
	}

	public function scopeWithSlug($query, $slug) {
		return $query->whereHas('series', function($q) use ($slug) {$q->where('slug', '=', $slug);});
	}

	public function getFormalTitle() {
		$title = $this->title;
		$number = $this->number;
		$series = $this->series->name;

		if ( $this->isFringe() && $this->hasParent() ) {
			$id = $this->id;
			$parent = $this->getParent();

			$slug = strtoupper($parent->series->slug);
			$pnumber = $parent->number;
			$title = "$slug $pnumber — $title";
		}

		$template = "$series #$number: $title";
		return $template;
	}

	public function getFormalTitleAttribute() {
		return $this->getFormalTitle();
	}

	private function _getRelated($target) {
		$related = $this->related;
		$related = $related->filter(function($item) use ($target) {
			if ( $item->pivot->type == $target ) {
				return true;
			}
			return false;
		});
		return $related->first();
	}

	public function getParent() {
		return $this->_getRelated('parent');
	}

	public function getFringe() {
		return $this->_getRelated('fringe');
	}

	public function isNew() {
		$created_at = $this->created_at;
		$against = strtotime("-7 days");
		return strtotime($created_at) > $against;
	}

	public function isFringe() {
		$series = $this->series->slug;
		return $series == 'tf';
	}

	public function hasFringe() {
		return $this->getFringe() != null;
	}

	public function hasParent() {
		return $this->getParent() != null;
	}

	public function mp3() {
		return $this->media->filter(function($v){return $v->type == 'mp3';})->first();
	}

	// public function save(array $options = []) {
	// 	parent::save($options);
	//
	// 	\Event::fire(new EpisodeWasSaved($this));
	// }

}
