<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model {

	protected $guarded = [
						'people_default',
						'art_list'
	];



	public function meta() {
		return $this->hasOne('App\SeriesMeta');
	}

	public function episodes() {
		return $this->hasMany('App\Episode');
	}

	public function hosts() {
		return $this->belongsToMany('App\Person', 'people_series', 'series_id', 'person_id');
	}

	public function art() {
		return $this->belongsToMany('App\AlbumArt', 'album_art_series', 'series_id', 'album_art_id');
	}

	public function getArtListAttribute() {
		return $this->art->lists('id')->toArray();
	}

	public function getPeopleDefaultAttribute() {
		return $this->hosts->lists('id')->toArray();
	}

	public function scopeVisible($query) {
		return $query->where('hidden', '=', false);
	}

	public function scopeUsable($query) {
		return $query
			->where('hidden', '=', false)
			->where('retired', '=', false)
			->where('hiatus', '=', false);
	}

	public function scopeWithSlug($query, $slug) {
		return $query->where('slug', '=', $slug);
	}


}
