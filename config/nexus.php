<?php

return [

	'title' => [
		'title' => 'The Nexus',
		'glue' => '&#8250;'
	],

	'episode' => [

		'states' => [
			'draft' => 'draft',
			'preview' => 'preview',
			'published' => 'published',
			'scheduled' => 'scheduled'
		]

	],

	'media' => [

		'types' => [
			'mp3' => 'mp3'
		]

	],

	'art' => [
		'location' => [
				'web' => '/art/'
		],
		'sizes' => [
				'small' => 300,
				'medium' => 800,
				'large' => 1400
			]
	]

];
