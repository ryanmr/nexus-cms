<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserSeeder extends Seeder {

	public function run() {

		DB::table('users')->delete();

		$users = [
			['name' => 'admin', 'email' => 'admin@thenexus.tv', 'password' => Hash::make('admin')],
			['name' => 'ryan', 'email' => 'ryan@ryanrampersad.com', 'password' => Hash::make('admin')]
		];

		DB::table('users')->insert($users);

	}

}
