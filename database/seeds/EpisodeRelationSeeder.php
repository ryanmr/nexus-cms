<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EpisodeRelationSeeder extends Seeder {

	public function run() {

		DB::table('episodes_relations')->delete();

		$json = File::get(storage_path() . '/json/seeds.json');
		$data = json_decode($json, true);

		$episodes = $data['episode_relations'];

		// $episodes = array_map(function($episode){
		// 	return $episode;
		// }, $episodes);

		DB::table('episodes_relations')->insert($episodes);

	}

}
