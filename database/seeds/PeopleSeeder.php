<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PeopleSeeder extends Seeder {

	public function run() {

		DB::table('people')->delete();

		$json = File::get(storage_path() . '/json/seeds.json');
		$data = json_decode($json, true);

		$people = $data['people'];

		$people = array_map(function($person){
			$person['slug'] = str_slug($person['name']);
			return $person;
		}, $people);

		DB::table('people')->insert($people);

	}

}
