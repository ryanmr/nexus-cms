<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EpisodeMediaSeeder extends Seeder {

	public function run() {

		DB::table('episodes_medias')->delete();

		$json = File::get(storage_path() . '/json/seeds.json');
		$data = json_decode($json, true);

		$media = $data['episode_medias'];

		$media = array_map(function($m){
			$parts = explode(':', $m['length']);
			$ws = [3600, 60, 1];
			$total = 0;
			for ($i = 0; $i < count($parts); $i++) {
				$w = $ws[$i];
				$total = $total + ($w * $parts[$i]);
			}
			$m['length'] = $total;
			return $m;
		}, $media);

		DB::table('episodes_medias')->insert($media);

	}

}
