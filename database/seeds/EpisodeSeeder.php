<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EpisodeSeeder extends Seeder {

	public function run() {

		DB::table('episodes')->delete();

		$json = File::get(storage_path() . '/json/seeds.json');
		$data = json_decode($json, true);

		$episodes = $data['episodes'];

		$episodes = array_map(function($episode){
			$episode['state'] = 'published';
			return $episode;
		}, $episodes);

		$episodes = array_map(function($episode){
			$episode['title'] = $episode['name'];
			unset($episode['name']);
			return $episode;
		}, $episodes);

		$episodes = array_map(function($episode){
			$episode['published_at'] = $episode['created_at'];
			return $episode;
		}, $episodes);

		DB::table('episodes')->insert($episodes);

	}

}
