<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PeopleEpisodeSeeder extends Seeder {

	public function run() {

		DB::table('people_episodes')->delete();

		$json = File::get(storage_path() . '/json/seeds.json');
		$data = json_decode($json, true);

		$people = $data['people_relations'];

		// $people = array_map(function($person){
		// 	return $person;
		// }, $people);

		DB::table('people_episodes')->insert($people);

	}

}
