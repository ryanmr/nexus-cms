<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class SeriesSeeder extends Seeder {

	public function run() {

		DB::table('series')->delete();
		DB::table('series_metas')->delete();

		$json = File::get(storage_path() . '/json/seeds.json');
		$data = json_decode($json, true);

		$series = $data['series'];

		// the long slug cannot be blank

		$series = array_map(function($s){
			$s['long_slug'] = Str::slug($s['name'], '-');
			$s['created_at'] = new DateTime();
			$s['updated_at'] = new DateTime();
			return $s;
		}, $series);

		/*
			Silent errors!
			All fields in the inserts must be present, or the entire insert will fail.
		*/

		$metas = array_map(function($s){
			$r = ['id' => $s['id'], 'series_id' => $s['id']];
			return $r;
		}, $series);


		DB::table('series')->insert($series);
		DB::table('series_metas')->insert($metas);

	}

}
