<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		/*
			Describes the basic details of a series.
		*/
		Schema::create('series', function(Blueprint $table){
			$table->increments('id');

			// formal name
			$table->string('name')->unique();

			// short name / slug (e.g. atn, eb, cs)
			$table->string('slug')->unique();

			// long slug (e.g. at-the-nexus, eight-bit, control-structure)
			$table->string('long_slug')->unique();

			// short description
			$table->text('description');

			$table->boolean('hidden')->default(false);
			$table->boolean('retired')->default(false);
			$table->boolean('hiatus')->default(false);

			$table->timestamps();
		});

		/*
			Describes the extra details for a series.
		*/
		Schema::create('series_metas', function(Blueprint $table){

			$table->increments('id');

			// content for the series landing page; ~/atn/ for exmaple
			$table->text('landing_content');

			// a centralized place for series specific attribution
			$table->text('attribution_content');

			$table->string('feed_author');

			$table->string('feed_geographic_location');
			$table->string('feed_episode_frequency');

			$table->string('feed_image_url');

			$table->string('feed_tracking_url');

			$table->string('google_play_subscription_url');

			$table->string('itunes_subscription_url');
			$table->text('itunes_summary');
			$table->string('itunes_keywords');
			$table->string('itunes_category_overall');
			$table->string('itunes_category_primary');
			$table->string('itunes_category_secondary');

			$table->string('itunes_email');
			$table->string('itunes_image_url');

			$table->boolean('itunes_explicit');

			$table->integer('series_id')->unsigned();
			$table->foreign('series_id')->references('id')
					->on('series')
					->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('series_metas', function(Blueprint $table) {
			$table->dropForeign('series_metas_series_id_foreign');
		});
		Schema::drop('series_metas');
		Schema::drop('series');
	}

}
