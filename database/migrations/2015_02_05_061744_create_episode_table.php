<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		/*
			Describes specific episodes.
		*/
		Schema::create('episodes', function(Blueprint $table){
			$table->increments('id');

			// primary methods of addressing the episode title
			$table->string('title');
			$table->string('subtitle');

			$table->integer('number');

			// episode description
			$table->text('description');

			// episode content: links, etc
			$table->text('content');

			// private notes for admin backend
			$table->text('notes');

			// external url to show notes
			$table->string('external_notes_url');

			// e.g. draft, preview, published, etc
			$table->string('state')->default('draft');

			// boolean flags
			$table->boolean('hidden')->default(false);
			$table->boolean('unlisted')->default(false);
			$table->boolean('nsfw')->default(false);

			$table->timestamps();
			$table->timestamp('published_at')->nullable();

			$table->integer('series_id')->unsigned();
			$table->foreign('series_id')->references('id')->on('series')->onDelete('cascade');

			// Describes the relationship between a specific episode and an album art set.
			$table->integer('album_art_id')->unsigned()->nullable()->default(null);
			$table->foreign('album_art_id')->references('id')->on('album_art')->onDelete('set null');;

			$table->softDeletes();
		});

		/*
			Describes the media that is attached to each episode.
		*/
		Schema::create('episodes_medias', function(Blueprint $table){
			$table->increments('id');

			// for mp3, youtube, etc
			$table->string('type');

			// for the actual content type
			$table->string('mime');

			// meta - for other special settings
			// hopefully it will use a json string
			$table->string('meta');

			$table->integer('length')->default(0);
			$table->integer('size')->default(0);
			$table->string('url');
			$table->boolean('hidden')->default(false);

			// media belongs to an episode
			$table->integer('episode_id')->unsigned();
			$table->foreign('episode_id')->references('id')->on('episodes')->onDelete('cascade');
		});

		/*
			Relate episodes to other episodes.
		*/
		Schema::create('episodes_relations', function(Blueprint $table){
			$table->increments('id');

			// name used for type specification, like
			// fringe, parent
			$table->string('type');

			$table->integer('episode_id')->unsigned();
			$table->foreign('episode_id')->references('id')->on('episodes')->onDelete('cascade');

			$table->integer('episode_related_id')->unsigned();
			$table->foreign('episode_related_id')->references('id')->on('episodes')->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('episodes_relations', function(Blueprint $table) {
			$table->dropForeign('episodes_relations_episode_id_foreign');
		});
		Schema::table('episodes_relations', function(Blueprint $table) {
			$table->dropForeign('episodes_relations_episode_related_id_foreign');
		});

		Schema::drop('episodes_relations');
		Schema::drop('episodes_medias');
		Schema::drop('episodes');
	}

}
