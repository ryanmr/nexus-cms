<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		/*
			Describes the people that have appeared on the network,
			in episodes.
		*/
		Schema::create('people', function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->string('email');

			$table->string('slug');

			$table->string('web_url');
			$table->string('twitter_url');
			// optionally, because gravatar might be used again?
			$table->string('avatar_url');

			$table->text('content');

			$table->boolean('hidden')->default(false);

			$table->timestamps();
		});

		/*
			Describes the relationship between people and episodes.
		*/
		Schema::create('people_episodes', function(Blueprint $table){

			$table->increments('id');

			// the default role will be guest
			$table->string('role')->default('guest');

			$table->integer('episode_id')->unsigned();
			$table->foreign('episode_id')->references('id')->on('episodes')->onDelete('cascade');

			$table->integer('person_id')->unsigned();
			$table->foreign('person_id')->references('id')->on('people')->onDelete('cascade');

		});

		/*
			Describes the default hosts (per episode) and the current hosts for a particular series.
		*/
		Schema::create('people_series', function(Blueprint $table){

			$table->increments('id');

			$table->integer('series_id')->unsigned();
			$table->foreign('series_id')->references('id')->on('series')->onDelete('cascade');

			$table->integer('person_id')->unsigned();
			$table->foreign('person_id')->references('id')->on('people')->onDelete('cascade');


		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('people_episodes', function(Blueprint $table) {
			$table->dropForeign('people_episodes_episode_id_foreign');
		});
		Schema::table('people_episodes', function(Blueprint $table) {
			$table->dropForeign('people_episodes_person_id_foreign');
		});

		Schema::table('people_series', function(Blueprint $table) {
			$table->dropForeign('people_series_series_id_foreign');
		});
		Schema::table('people_series', function(Blueprint $table) {
			$table->dropForeign('people_series_person_id_foreign');
		});

		Schema::drop('people_series');
		Schema::drop('people_episodes');
		Schema::drop('people');
	}

}
