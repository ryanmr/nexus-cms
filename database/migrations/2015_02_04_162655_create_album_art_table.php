<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlbumArtTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		/*
			Describes a named collection of album art.
		*/
		Schema::create('album_art', function(Blueprint $table){
			$table->increments('id');

			// formal name
			$table->string('name');
			$table->boolean('hidden')->default(false);
			$table->boolean('legacy')->default(false);

			$table->timestamps();
		});

		/*
			Individual copies of the same album art view.
		*/
		Schema::create('album_art_views', function(Blueprint $table){

			$table->increments('id');
			$table->timestamps();

			$table->string('name');
			$table->string('filename');

			$table->string('path');

			$table->integer('width');
			$table->integer('height');

			$table->boolean('hidden')->default(false);

			$table->integer('album_art_id')->unsigned();
			$table->foreign('album_art_id')->references('id')->on('album_art');

		});

		/*
			Describes the relationship between a series and a named album art set.
		*/
		Schema::create('album_art_series', function(Blueprint $table){
			$table->increments('id');

			$table->integer('album_art_id')->unsigned();
			$table->foreign('album_art_id')->references('id')->on('album_art');

			$table->integer('series_id')->unsigned();
			$table->foreign('series_id')->references('id')->on('series');


		});

		// Album Art for an episode is specified on the episode;
		// it is specified with a nullable key also.

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('album_art_views', function(Blueprint $table) {
			$table->dropForeign('album_art_views_album_art_id_foreign');
		});

		// Album Art for Series
		Schema::table('album_art_series', function(Blueprint $table) {
			$table->dropForeign('album_art_series_album_art_id_foreign');
		});
		Schema::table('album_art_series', function(Blueprint $table) {
			$table->dropForeign('album_art_series_series_id_foreign');
		});

		Schema::drop('album_art_series');
		Schema::drop('album_art_views');
		Schema::drop('album_art');
	}

}
