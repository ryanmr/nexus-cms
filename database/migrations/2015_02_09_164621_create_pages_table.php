<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){

		/*
			Describes a pages.
		*/
		Schema::create('pages', function(Blueprint $table){
			$table->increments('id');

			// formal name
			$table->string('name');
			$table->string('slug');
			$table->text('description');
			$table->text('content');

			$table->boolean('hidden')->default(false);

			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
